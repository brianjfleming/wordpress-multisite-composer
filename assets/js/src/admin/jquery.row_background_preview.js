
$( document ).ready( function() {
    init_row_background_preview();
} );

/** Row Background Preview **/
$.row_background_preview = function( el ) {

    var preview_pane = $(el);
        preview_pane.values = {};

    var methods = {

        init: function() {

            methods.preview(); // Initial Preview call

        },

        preview: function() {

            var settings = getRowValues();

            // Ajax vars
            var postVars = {
                action: 'layouts_ajax',
                preview_background: settings.data,
                security: $('#security').val()
            };

            // Send to ajax and get the response
            $.post(ajaxurl, postVars, function ( response ) {
                preview_pane.replaceWith( response );
                if ( settings.data['background-behavior'] == 'row-slider' || settings.data['container-background-behavior'] == 'row-slider' ){
                    slidebarSlider();
                }
            });
        }

    };

    methods.init();
};

$.fn.row_background_preview = function() {
    return this.each(function() {
        new $.row_background_preview(this);
    });
};

function init_row_background_preview() {

    // Load for widgets when added and updated
    $('div#background-preview').row_background_preview();

    // Trigger background preview from repeater
    $( document ).on( 'bf-repeater-sort background-preview-update bf-repeater-remove', function() {
        $('div#background-preview').row_background_preview();
    } );

    // Trigger background preview with global options
    $('#custom-classes, #background-behavior, #animation, #slideshowSpeed, #animationSpeed, #container-background-behavior, #container-animation, #container-slideshowSpeed, #container-animationSpeed, #container-classes, #container-desktop-classes, #container-mobile-classes').on( 'change', function(){
        $('div#background-preview').row_background_preview();
    });
}