
$( document ).ready( function () {

    saveTemplate();
    deleteTemplate();
    toggleTemplateRepeater();

    $( document ).on( 'bf-repeater-sort bf-repeater-remove', function() {
        init_row_select();
    });

    $( document ).on( 'change', '#header select, #content select, #footer select', function() {
        init_row_select();
    });

} );

function getTemplateRows(){

    var allRows = {
        headerRows:[],
        contentRows:[],
        footerRows:[],
    };

    $('#header').find( 'select' ).each( function() {
        if( '' !== $(this).val() ) {
            allRows.headerRows.push($(this).val());
        }
    });

    $('#content').find( 'select' ).each( function() {
        if( '' !== $(this).val() ) {
            allRows.contentRows.push($(this).val());
        }
    });

    $('#footer').find( 'select' ).each( function() {
        if( '' !== $(this).val() ) {
            allRows.footerRows.push($(this).val());
        }
    });

    return allRows;
}

function saveTemplate() {

    $(document).on( 'click', '#save-template' , function() {

        var assignments = [];
            allRows = getTemplateRows();

        $('#assignments').find('.assignment').each(function () {
            if ($(this).prop('checked')) {
                assignments.push($(this).val());
            }
        });

        // All settings from the page set into an object
        settings = {
            'id': $('#id').val(),
            'data': {
                'name': $('#name').val(),
                'description': $('#description').val(),
                'header' : allRows.headerRows,
                'content' : allRows.contentRows,
                'footer' : allRows.footerRows,
                'assignments' : assignments,
                'sidebar_col' : $('#sidebarcol').find('select').val(),
                'sidebar_row' : $('#contentrow-select').val(),
                'fixed_row'   : $('#fixedrow').find('select').val()
            }
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            save_template: settings,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function (response) {

            $('h1').text( response.title );
            history.pushState(null, null, response.history );
            // Lock the assignments
            $('#assignments').find('.assignment').each(function () {
                if ($(this).prop('checked')) {
                    $(this).attr('disabled', true);
                }
            });
            messageHandler( response.success );
        });
    });
}

function deleteTemplate() {
    $(document).on( 'click', '#delete-template' , function() {

        var deleteInfo = {
            id : $('#id').val(),
            name : $('#name').val(),
        };

        // Ajax vars
        postVars = {
            action: 'layouts_ajax',
            delete_template: deleteInfo,
            security: $('#security').val(),
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            location.href = response;
        });

    });
}

function toggleTemplateRepeater() {

    $(document).on( 'change', '#contentrow select, #sidebarcol select', function(){

        var contentRow = $('#contentrow').find('select').val();
        var sidebarCol = $('#sidebarcol').find('select').val();

        $('.sidebar-col').removeClass('sidebar-col');

        if ( contentRow && sidebarCol ){
            $(document).find( '.row-' + contentRow + ' .' + sidebarCol ).addClass('sidebar-col');
        }
    });

    $(document).on( 'change', '.admin_page_stellar-template-edit .bf-repeater-inside select', function(){

        var selectedRow = $(this).val();
        var titlebar = $(this).parents('.bf-repeater').find('.bf-repeater-top');
        var postVars = {
            action: 'repeater',
            rowSelected: selectedRow
        };

        // Send to ajax and get the response
        $.post(ajaxurl, postVars, function ( response ) {
            titlebar.html('');
            titlebar.append(response).hide().fadeIn(300);
        });

    });
}

function init_row_select() {

    var rows_change = {
        'rows': getTemplateRows(),
        'content_selected': $('#contentrow').find('select').val(),
        'fixed_selected': $('#fixedrow').find('select').val()
    };

    // Ajax vars
    postVars = {
        action: 'layouts_ajax',
        rows_change: rows_change,
        security: $('#security').val(),
    };

    // Send to ajax and get the response
    $.post(ajaxurl, postVars, function (response) {
        $('#contentrow-select').html(response.content);
        $('#fixedrow-select').html(response.fixed);
    });
}