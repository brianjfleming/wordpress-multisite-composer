(function($){
	
	jQuery.fn.bfmenu = function(options){
		var settings;
		$.extend( settings = {
			showSpeed: 300,
			hideSpeed: 300,
			trigger: "hover",
			showDelay: 250,
			hideDelay: 250,
			effect: "fade",
			align: "left",
			responsive: true,
			indentChildren: false,
			indicatorFirstLevel: "fa-caret-right",
			scrollable: false,
			scrollableMaxHeight: 400,
			mobileMenuText: 'MENU'
		}, options);

		// variables
		var menu_container = $(this);
		var menu = $(menu_container).children(".bfmenu-menubar");
		var menu_li = $(menu).find("li");
		var showHideButton;
		var mobileWidthBase = 768;
		var bigScreenFlag = 2000; // a number greater than "mobileWidthBase"
		var smallScreenFlag = 200; // a number less than "mobileWidthBase"
		
		// dropdown/megamenu indicators
		$(menu).children("li").children("a").each(function(){
			if($(this).siblings(".dropdown, .megamenu").length > 0){
				$(this).append('<i class="indicator fa ' +  settings.indicatorFirstLevel + '" aria-hidden="true"></i>');
			}
		});
		$(menu).find(".dropdown").children("li").children("a").each(function(){
			if($(this).siblings(".dropdown").length > 0){
				$(this).append('<i class="indicator fa ' +  settings.indicatorFirstLevel + '" aria-hidden="true"></i>');
			}
		});

		
		// responsive behavior
		if(settings.responsive){ 
			$(menu_container).prepend('<a href="javascript:void(0)" class="showhide"><span>' +  settings.mobileMenuText + '</span><i class="fa fa-bars" aria-hidden="true"></i></a>');
			showHideButton = $(menu_container).children(".showhide");
		}

		// scrollable menu
		if(settings.scrollable){
			if(settings.responsive){
				$(menu).css("max-height", settings.scrollableMaxHeight).addClass("scrollable").append("<li class='scrollable-fix'></li>");
			}
		}
		
		// shows a dropdown
		function showDropdown(item){
			$(item).addClass("active");
			if(settings.effect == "fade")
				$(item).children(".dropdown, .megamenu").stop(true, true).delay(settings.showDelay).fadeIn(settings.showSpeed).addClass('active');
			else
				$(item).children(".dropdown, .megamenu").stop(true, true).delay(settings.showDelay).slideDown(settings.showSpeed).addClass('active');
		}
		
		// hides a dropdown
		function hideDropdown(item){
			$(item).removeClass("active"); // We don't want this to affect tab-items
			if(settings.effect == "fade")
				$(item).children(".dropdown, .megamenu").stop(true, true).delay(settings.hideDelay).fadeOut(settings.hideSpeed).removeClass('active');
			else
				$(item).children(".dropdown, .megamenu").stop(true, true).delay(settings.hideDelay).slideUp(settings.hideSpeed).removeClass('active');
				$(item).children(".dropdown, .megamenu").find(".dropdown, .megamenu").stop(true, true).delay(settings.hideDelay).fadeOut(settings.hideSpeed);
		}
		
		// landscape mode
		function landscapeMode(){
			$(menu).find(".dropdown, .megamenu").hide(0);
			if(navigator.userAgent.match(/Mobi/i) || window.navigator.msMaxTouchPoints > 0 || settings.trigger == "click"){
				$(".bfmenu-menubar > li > a, .bfmenu-menubar ul.dropdown li a").bind("click touchstart", function(e){
					e.stopPropagation();
					e.preventDefault();
					$(this).parent("li").siblings("li").removeClass('active').find(".dropdown, .megamenu").stop(true, true).fadeOut(300);
					if($(this).siblings(".dropdown, .megamenu").css("display") == "none"){
						showDropdown($(this).parent("li"));
						return false;
					}
					else{
						hideDropdown($(this).parent("li"));
					}
					window.location.href = $(this).attr("href");
				});
				$(document).bind("click.menu touchstart.menu", function(ev){
					if($(ev.target).closest(".bfmenu").length == 0){
						$(".bfmenu-menubar").find("li").removeClass('active');
						$(".bfmenu-menubar").find(".dropdown, .megamenu").fadeOut(300);
					}
				});
			}
			else{
				$(menu_li).bind("mouseenter", function(){
					showDropdown(this);
				}).bind("mouseleave", function(){
					hideDropdown(this);
				});
			}
		}
		
		// portrait mode
		function portraitMode(){
			$(menu).find(".dropdown, .megamenu").hide(0);
			$(menu).find(".indicator").each(function(){
				if($(this).parent("a").siblings(".dropdown, .megamenu").length > 0){
					$(this).bind("click", function(e){

						var clickedElement = $(this);

						if($(this).parent().prop("tagName") == "A"){
							e.preventDefault();
						}
						if($(this).parent("a").siblings(".dropdown, .megamenu").css("display") == "none"){
							$(this).parent("a").siblings(".dropdown, .megamenu").delay(settings.showDelay).slideDown({
								duration: settings.showSpeed,
								start: function() {
									$(this).siblings("a").find(".indicator").css({
										'transform': 'rotate(90deg)',
										'transition': 'all ' + settings.showSpeed + 'ms'
									});
									$('html, body').animate({
										scrollTop: $(clickedElement).offset().top
									}, 500);
								}
							});
							$(this).parent("a").parent("li").siblings("li").find(".dropdown, .megamenu").slideUp({
								duration: settings.hideSpeed,
								start: function() {
									$(this).parent("li").find(".indicator").css({
										'transform': 'rotate(0)',
										'transition': 'all ' + settings.hideSpeed + 'ms'
									});
								}
							});
							$(this).closest('li').siblings().removeClass('active');
							$(this).closest('li').addClass('active');

						}
						else{
							$(this).parent("a").siblings(".dropdown, .megamenu").slideUp({
								duration: settings.hideSpeed,
								start: function() {
									$(this).parent("li").find(".indicator").css({
										'transform': 'rotate(0)',
										'transition': 'all ' + settings.hideSpeed + 'ms'
									});
								}
							});
							$(this).parent("a").siblings("ul").find(".dropdown, .megamenu").slideUp({
								duration: settings.hideSpeed,
								start: function() {
									$(this).parent("li").find(".indicator").css({
										'transform': 'rotate(0)',
										'transition': 'all ' + settings.hideSpeed + 'ms'
									});
								}
							});
							$(this).closest('li').removeClass('active');
						}
					});
				}
			});
		}
		
		// Fix the submenu on the right side
		function fixSubmenuRight(){
			var submenus = $(menu).children("li").children(".dropdown, .megamenu");
			if($(window).innerWidth() > mobileWidthBase){
				var menu_width = $(menu_container).outerWidth(true);
				for(var i = 0; i < submenus.length; i++){
					if($(submenus[i]).parent("li").position().left + $(submenus[i]).outerWidth() > menu_width){
						$(submenus[i]).css("right", 0);
						$(submenus[i]).find(".dropdown").addClass('dropdown-left');
					}
					else{
						if(menu_width == $(submenus[i]).outerWidth() || (menu_width - $(submenus[i]).outerWidth()) < 20){
							$(submenus[i]).css("left", 0);
						}
						if($(submenus[i]).parent("li").position().left + $(submenus[i]).outerWidth() < menu_width){
							$(submenus[i]).css("right", "auto");
						}
					}
				}
			}
		}
		
		// show the bar to show/hide menu items on mobile
		function showMobileBar(){
			$(menu).hide(0);
			$(showHideButton).show(0).click(function(){
				if ($(menu).css("display") == "none") {
					$(menu).slideDown(settings.showSpeed);
				}
				else {
					$(menu).slideUp(settings.hideSpeed).find(".dropdown, .megamenu").hide(settings.hideSpeed);
					$(menu).find('li').removeClass('active');
					$(menu).find(".indicator").css( { 'transform' : 'rotate(0)' });
				}
			});
		}
		
		// hide the bar to show/hide menu items on mobile
		function hideMobileBar(){
			$(menu).show(0);
			$(showHideButton).hide(0);
		}
		
		// unbind events
		function unbindEvents(){
			$(menu_container).find("li, a").unbind();
			$(document).unbind("click.menu touchstart.menu");
		}

		function menuTabs() {

			$(".bfmenu-tab-content").hide();
			$(".bfmenu-tab-content:first").show();

			/* if in tab mode */
			$("li.tab-nav-item > a").unbind('click touchstart').bind("click touchstart", function (event) {
				event.stopPropagation();
				event.preventDefault();

				$(".bfmenu-tab-content").hide();
				var activeTab = $(this).attr("rel");
				$("#" + activeTab).show();

				$("li.tab-nav-item a").removeClass("active");
                $("li.tab-nav-item").removeClass("active");
                $(this).addClass("active");
                $(this).parents("li").addClass("active");

				$(".bfmenu-tab-heading").removeClass("active");
				$(".bfmenu-tab-heading[rel^='" + activeTab + "']").addClass("active");
			});

			/* if in accordion mode */
			$(".bfmenu-tab-heading").unbind('click touchstart').bind("click touchstart", function (event) {
				event.stopPropagation();
				event.preventDefault();

				var accordion_activeTab = $(this).attr("rel");
				var isActive = $(this).hasClass("active");

				$(".bfmenu-tab-heading").removeClass("active");
				$("li.tab-nav-item a").removeClass("active");

				if ( isActive ) {
					$("#" + accordion_activeTab).slideUp( settings.showSpeed ).removeClass("active");
					$(this).removeClass("active");
				} else {
					$(".bfmenu-tab-content").hide();
					$("#" + accordion_activeTab).slideDown( settings.showSpeed ).addClass("active");
					$(this).addClass("active");
					$("li.tab-nav-item a[rel^='" + accordion_activeTab + "']").addClass("active");
					$('html, body').animate({
						scrollTop: $(this).offset().top
					}, 500);
				}
			});
		}


		// return window's width
		function windowWidth(){
			return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		}
		
		// navigation start function
		function startMenu(){
			fixSubmenuRight();
			if(windowWidth() <= mobileWidthBase && bigScreenFlag > mobileWidthBase){
				unbindEvents();
				if(settings.responsive){
					showMobileBar();
					portraitMode();
				}
				else{
					landscapeMode();
				}
			}
			if(windowWidth() > mobileWidthBase && smallScreenFlag <= mobileWidthBase){
				unbindEvents();
				hideMobileBar();
				landscapeMode();
			}
			bigScreenFlag = windowWidth();
			smallScreenFlag = windowWidth();
			menuTabs();
			/* IE8 fix */
			if(/MSIE (\d+\.\d+);/.test(navigator.userAgent) && windowWidth() < mobileWidthBase){
				var ieversion = new Number(RegExp.$1);
				if(ieversion == 8){
					$(showHideButton).hide(0);
					$(menu).show(0);
					unbindEvents();
					landscapeMode();
				}
			}
		}		
		
		startMenu();
		$(window).resize(function(){
			startMenu();
			fixSubmenuRight();
		});
		
	}
}(jQuery));

/**
* jquery.scrollTo
* Copyright (c) 2007-2014 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
* Licensed under MIT
* @author Ariel Flesler
* @version 1.4.13
*/

(function(k){'use strict';k(['jquery'],function($){var j=$.scrollTo=function(a,b,c){return $(window).scrollTo(a,b,c)};j.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:!0};j.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(f,g,h){if(typeof g=='object'){h=g;g=0}if(typeof h=='function')h={onAfter:h};if(f=='max')f=9e9;h=$.extend({},j.defaults,h);g=g||h.duration;h.queue=h.queue&&h.axis.length>1;if(h.queue)g/=2;h.offset=both(h.offset);h.over=both(h.over);return this._scrollable().each(function(){if(f==null)return;var d=this,$elem=$(d),targ=f,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=win?$(targ):$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}var e=$.isFunction(h.offset)&&h.offset(d,targ)||h.offset;$.each(h.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=j.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(h.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=e[pos]||0;if(h.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*h.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(h.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&h.queue){if(old!=attr[key])animate(h.onAfterFirst);delete attr[key]}});animate(h.onAfter);function animate(a){$elem.animate(attr,g,h.easing,a&&function(){a.call(this,targ,h)})}}).end()};j.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return $.isFunction(a)||typeof a=='object'?a:{top:a,left:a}}return j})}(typeof define==='function'&&define.amd?define:function(a,b){if(typeof module!=='undefined'&&module.exports){module.exports=b(require('jquery'))}else{b(jQuery)}}));