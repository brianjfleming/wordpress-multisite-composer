var $ = jQuery.noConflict();

if( typeof stellarPalettes != 'undefined' ) {

    $.wp.wpColorPicker.prototype.options = { palettes: stellarPalettes, hide: true };

}