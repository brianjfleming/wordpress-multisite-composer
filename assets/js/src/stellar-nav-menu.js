/**
 * Globals
 */
var $ = jQuery.noConflict();
var api = 'NULL';
if( typeof wpNavMenu !== 'undefined' ) {
    api = wpNavMenu;
}

/**
 * Document Ready
 */
$(document).ready(function () {

    if( 'NULL' != api ) {
        extendWpNavMenu();
    }

});

/**
 * Extends the WPNavMenu object to allow our Stellar Menu Item to work
 */
function extendWpNavMenu() {

    bfNavMenu = {

        // Meta Box Process Attachemnts
        stellarAddAnchor : function( processMethod ) {

            var label = $('#stellar-menu-item-name').val();
            var url = '#';

            processMethod = processMethod || api.addMenuItemToBottom;

            if ( '' === label ) {
                $('#stellarcustomlinkdiv').addClass('form-invalid');
                return false;
            }

            // Show the ajax spinner
            $( '.stellarcustomlinkdiv .spinner' ).addClass( 'is-active' );

            this.addLinkToMenu( url, label, processMethod, function() {
                // Remove the ajax spinner
                $( '.stellarcustomlinkdiv .spinner' ).removeClass( 'is-active' );
                // Set custom link form back to defaults
                $('#stellar-menu-item-name').val('').blur();
            });
        },

        // Meta Box Listeners
        stellarAttachPanelListeners : function() {

            $('#menu-settings-column').bind('click', function(e) {
                var target = $(e.target);
                if( target.hasClass('submit-add-to-menu') ) {
                    api.registerChange();

                    // Stellar Anchor Tag Add
                    if (e.target.id && 'submit-stellarcustomlinkdiv' == e.target.id) {

                        api.stellarAddAnchor(api.addMenuItemToBottom);
                    }
                }
            });
        }

    };

    // Extend wpNavMenu with our additional object
    $.extend( true, wpNavMenu, bfNavMenu );

    // Execute our listeners
    api.stellarAttachPanelListeners();

}