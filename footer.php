<?php
/**
 * The template for displaying the footer.
 *
 * @package BrightFire Stellar
 * @since 0.1.0
 */
?>
	<?php wp_footer();?>
	<?php echo \BrightFire\Theme\Stellar\custom_scripts( 'footer' )?>
</body>
</html>