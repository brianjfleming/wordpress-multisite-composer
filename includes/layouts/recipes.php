<?php
namespace BrightFire\Theme\Stellar;

/**
 * Returns array of all recipes.
 *
 * @return array
 */
function get_recipes() {

	global $wpdb ;

	$all_recipes[ 'site' ] = $wpdb->get_results( "SELECT option_value FROM $wpdb->options WHERE option_name LIKE 'stellar_recipe_%'", ARRAY_N );
	$all_recipes[ 'network' ] = $wpdb->get_results( "SELECT meta_value FROM {$wpdb->base_prefix}sitemeta WHERE meta_key LIKE 'stellar_recipe_%'", ARRAY_N );

	foreach( $all_recipes[ 'site' ] as $key => $data ) {
		$all_recipes[ 'site' ][ $key ] = maybe_unserialize( $data[ 0 ] );
	}

	foreach( $all_recipes[ 'network' ] as $key => $data ) {
		$all_recipes[ 'network' ][ $key ] = maybe_unserialize( $data[ 0 ] );
	}

	return $all_recipes;
}


/**
 * Switch for getting values from the "Network ( multi site )" level or "Blog ( single site )" level in WordPress.
 * A globally available option is 'network' where a single "blog" or single site option is 'site'.
 *
 * @param string $scope     Can be 'network' or 'site'.
 * @param string $option    Name of option. The literal 'option_name' in the WordPress options table.
 *
 * @return bool|mixed|void
 */
function get_scoped_option( $id, $scope ) {

	$value = false;

	if ( 'network' == $scope ) {

		$value = get_site_option( $id );

	} elseif ( 'site' == $scope ) {

		$value = get_option( $id );

	}

	return $value;
}

/**
 * Switch for saving values from the "Network ( multi site )" level or "Blog ( single site )" level in WordPress.
 * A globally available option is 'network' where a single "blog" or single site option is 'site'.
 *
 * @param string        $scope  Can be 'network' or 'site'.
 * @param string        $option Name of option. The literal 'option_name' in the WordPress options table.
 * @param string|int    $value Value to save.
 *
 * @return bool|mixed|void
 */
function update_scoped_option( $scope, $option, $value ) {

	if ( 'network' == $scope ) {

		$value = update_site_option( $option, $value );

	} elseif ( 'site' == $scope ) {

		$value = update_option( $option, $value );

	}

	return $value;
}

/**
 * Switch for deleting values from the "Network ( multi site )" level or "Blog ( single site )" level in WordPress.
 * A globally available option is 'network' where a single "blog" or single site option is 'site'.
 *
 * @param string $scope     Can be 'network' or 'site'.
 * @param string $option    Name of option. The literal 'option_name' in the WordPress options table.
 *
 */
function delete_scoped_option( $id, $scope ) {

	if ( 'network' == $scope ) {

		delete_site_option( $id );

	} elseif ( 'site' == $scope ) {

		delete_option( $id );

	}
}

/**
 * Returns selector for current theme recipes
 *
 * @param null $select_id string    HTML ID property
 *
 * @return string
 */
function recipe_selector( $select_id = null ) {

	$all_recipes = get_recipes();

	$selector_id = $select_id ? 'id="' . $select_id . '"' : '';
	$selector_name = $select_id ? 'name="' . $select_id . '"' : '';

	$output = '<select ' . $selector_id . ' ' . $selector_name . '><option disabled selected class="default">Select a recipe</option>';

	if ( is_array( $all_recipes['site'] ) ) {

		$output .= '<optgroup label="Site" data-scope="site">';

		foreach ( $all_recipes['site'] as $recipe ) {
			
			$output .= '<option value="' . $recipe[ 'recipe_id' ] . '">' . $recipe[ 'recipe_name' ] . '</option>';
		}

		$output .= '</optgroup>';
	}

	if ( ( 'export-recipe-selector' == $select_id || 'load-recipe-selector' == $select_id || 'view-recipe-selector' == $select_id ) || ( 'delete-recipe-selector' == $select_id && can_crud_network() ) ) {

		if ( is_array( $all_recipes['network'] ) ) {

			$output .= '<optgroup label="Network" data-scope="network">';

			foreach ( $all_recipes['network'] as $recipe ) {
				
				$output .= '<option value="' . $recipe[ 'recipe_id' ] . '">' . $recipe[ 'recipe_name' ] . '</option>';
			}

			$output .= '</optgroup>';
		}
	}

	$output .= '</select>';

	return $output;
}

/**
 * Users we want to be allowed to save and delete from network.
 *
 * @see recipe_selector()
 * @see admin.php
 *
 * @return bool
 */
function can_crud_network() {

	// User IDs that we allow access to the Network Level SAVE function
	$allowed_user_ids = array(
		'1', // brightfire - (AKA Matt McKenny)
//		'3', // Michael Garner
		'4', // Nathan Powell
//		'5', // Shawn Jenks
//		'146', // Greg Mercer
		'341', // Bob Whitis
		'1240', // Brian Flemming
	);

	$current_user_id = get_current_user_id();

	$is_allowed = in_array( $current_user_id, $allowed_user_ids );

	if ( $is_allowed && current_user_can( 'manage_sites' ) && is_multisite() ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Handles the recipe data on LOAD
 * 
 * @param $recipe
 */
function load_recipe( $recipe ) {

	// Grab our custom logo
	$custom_logo = ( get_theme_mod( 'custom_logo' ) ) ? get_theme_mod( 'custom_logo' ) : '';

	$recipe = recipe_image_load( $recipe );

	// Theme Mods
	update_option( 'theme_mods_brightfire-stellar', $recipe[ 'theme_mods' ] );

	// Custom Logo
	set_theme_mod( 'custom_logo', $custom_logo );

	// Mixes
	update_option( 'stellar_mixes', $recipe[ 'mixes' ] );

	// Sidebars
	wp_set_sidebars_widgets( $recipe[ 'sidebars' ] );

	// Widgets
	foreach ( $recipe[ 'widget_settings' ] as $widget => $settings ){
		update_option( $widget, $settings );
	}

	// Layouts
	update_option( 'stellar_layout', $recipe['layout'] );

	\BrightFire\Theme\Stellar\delete_stellar_css();
}

/**
 * Saves image data with recipe
 * 
 * @param $recipe
 *
 * @return mixed
 */
function recipe_image_save( $recipe ) {

	array_walk_recursive( $recipe, function( &$data, &$key ){

		if ( ( $key === 'image_id' || $key === 'body-background-image' ) && !empty( $data ) ) {

			$image = get_post( $data );
			$image_path = wp_get_attachment_image_src( $data, 'full' )[0];

			$new_image = array(
				'original_id'       => $data,
				'filename'          => basename( $image_path ),
				'image_path'        => $image_path,
				'post_mime_type'    => $image->post_mime_type,
				'post_title'        => $image->post_title,
				'post_content'      => '',
				'post_status'       => 'inherit'
			);

			$data = json_encode( $new_image );
		}
	} );

	return $recipe;
}

/**
 * Handles import/load of image data
 * 
 * @param $recipe
 *
 * @return mixed
 */
function recipe_image_load( $recipe ) {

	// Global container for processed images
	global $possible_duplicates;
	$possible_duplicates = array();

	// Check if we are on the same site and same environment for image import
	$same_site = ( BF_ENV == $recipe[ 'original_env' ] && get_current_blog_id() == $recipe[ 'original_blog' ] ) ? true : '';

	// Recurse recipe to check for images
	$recipe = recipe_image_load_recurse( $recipe, $recipe[ 'import_images' ], $same_site );

	return $recipe;
}

/**
 * Recurse recipe data to get images where needed if possible
 *
 * @param $recipe
 * @param $import
 * @param $same_site
 *
 * @return mixed
 */
function recipe_image_load_recurse( $recipe, $import, $same_site ) {

	// Get our duplicates array
	global $possible_duplicates;

	foreach ( $recipe as $key => $value ) {

		//
		if ( ( $key === 'image_id' || $key === 'body-background-image' ) && !empty( $value ) ) {

			// Decode data and typeset to array
			$import_data = (array)json_decode( $value );

			// Check if we are on the site the recipe was created on
			if ( $same_site ) {

				// No need to replace our original image
				$recipe[ $key ] = $import_data[ 'original_id' ];

				// Check if we are trying to import
			} elseif( $import ) {

				if ( array_key_exists( $import_data[ 'original_id' ], $possible_duplicates ) ) {

					// Do not duplicate images
					$recipe[ $key ] = $possible_duplicates[ $import_data[ 'original_id' ] ];
					
				} else {

					// setup a request object
					$image = new \WP_Http();

					// grab the data we need from the request
					$image = $image->request( $import_data[ 'image_path' ] );

					// make sure our image can be retrieved
					$acceptable_content = array( 'image/jpeg', 'image/png', 'image/gif' );

					if ( is_wp_error( $image ) || 200 !== $image[ 'response' ][ 'code' ] || !in_array( $image[ 'headers' ][ 'content-type' ], $acceptable_content, TRUE ) ) {

						//********************** @TODO: Image not found: image upload popup?
						$recipe[ $key ] = 'missing';

					} else {

						// begin upload and return the absolute path to our new file
						$new_image = wp_upload_bits( $import_data[ 'filename' ], null, $image[ 'body' ] );

						// Set data if this fails
						if ( !empty( $new_image[ 'error' ] ) ) {

							//********************** @TODO: uploaded image failed, what now?
							$recipe[ $key ] = 'missing';

						} else {

							// Our new filename (relative path)
							$filename = $new_image[ 'file' ];

							// FINALLY make a new post since we have the image
							$new_image_id = wp_insert_attachment( $import_data, $filename );

							// Generate thumbnails etc....
							$attachment_meta = wp_generate_attachment_metadata( $new_image_id, $filename );

							// Save some meta
							wp_update_attachment_metadata( $new_image_id,  $attachment_meta );

							// Set our new ID and add to our $possible_duplicates array
							$recipe[ $key ] = $possible_duplicates[ $import_data[ 'original_id' ] ] = $new_image_id;
						}
					}
				}

				// Assign missing images
			} else {

				$recipe[ $key ] = 'missing';
			}

			// Recurse to to find more images
		} elseif ( is_array( $recipe[ $key ] ) ) {

			$recipe[ $key ] = recipe_image_load_recurse( $recipe[ $key ], $import, $same_site );

		}
	}

	return $recipe;
}

function recipe_import_export_handler() {

	// If requested, forces download of recipe file export
	if( isset($_POST['export-recipe-selector']) ) {
		$id = $_POST[ 'export-recipe-selector' ];
		$scope = $_POST[ 'export-recipe-scope' ];

		// Recipe data
		$recipe = get_scoped_option( $id, $scope );

		// Make a File
		$file_contents = json_encode( $recipe );
		$file_name = '_recipe-' . str_replace( ' ', '_', $recipe[ 'recipe_name' ] ) . '.txt';

		header("Content-Type: text/plain");
		header("Content-disposition: attachment; filename=\"{$file_name}\"");
		echo $file_contents;

		exit();
	}

	// If requested, upload recipe file and set as active
	if( isset( $_FILES['import-recipe-file'] ) ) {

		$recipe_json = file_get_contents($_FILES['import-recipe-file']['tmp_name']);
		$recipe = json_decode( $recipe_json, true );

		$recipe[ 'import_images' ] = '';

		if ( isset( $_POST['image-upload-import'] ) ) {
			$recipe[ 'import_images' ] = true;
		}

		load_recipe( $recipe );

		?>

		<div class="notice notice-success is-dismissible">
			<p><?php _e( 'Uploaded Recipe [' . $recipe[ 'recipe_name' ] . '] was successfully loaded.', 'bf_stellar' ); ?></p>
		</div>

		<?php
	}
}