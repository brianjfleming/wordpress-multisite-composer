<?php

namespace BrightFire\Theme\Stellar;

/**
 * @return array
 */
function default_layout_array() {
	return array(
		'sidebars'    => array(),
		'menu_panels' => array(),
		'rows'        => array(
			array(
				'id'   => 'header',
				'data' => array(
					'name'        => 'Header',
					'description' => 'Main header row.',
					'usage'       => 'header',
					'left'        => '3',
					'right'       => '9',
				)

			),
			array(
				'id'   => 'menu',
				'data' => array(
					'name'        => 'Menu',
					'description' => 'Default menu row.',
					'usage'       => 'menu',
					'left'        => '12',
					'right'       => '0',
				)

			),
			array(
				'id'   => 'home-row-one',
				'data' => array(
					'name'        => 'Home Row One',
					'description' => 'Full width row.',
					'usage'       => 'content',
					'left'        => '12',
					'right'       => '0',
				)
			),
			array(
				'id'   => 'home-row-two',
				'data' => array(
					'name'        => 'Home Row Two',
					'description' => 'Two thirds one third row.',
					'usage'       => 'content',
					'left'        => '8',
					'right'       => '4',
				)
			),
			array(
				'id'   => 'home-row-three',
				'data' => array(
					'name'        => 'Home Row Three',
					'description' => 'Full width row.',
					'usage'       => 'content',
					'left'        => '12',
					'right'       => '0',
				)
			),
			array(
				'id'   => 'content-sidebar',
				'data' => array(
					'name'        => 'Posts & Pages',
					'description' => 'Post and page content with sidebar area.',
					'usage'       => 'content',
					'left'        => '8',
					'right'       => '4',
				)
			),
			array(
				'id'   => 'content',
				'data' => array(
					'name'        => 'Content Only',
					'description' => 'Full width row for use without a header or footer.',
					'usage'       => 'content',
					'left'        => '12',
					'right'       => '0'
				)
			),
			array(
				'id'   => 'footer',
				'data' => array(
					'name'        => 'Footer',
					'description' => 'Main footer row.',
					'usage'       => 'footer',
					'left'        => '12',
					'right'       => '0'
				)
			),
		),
		'templates'   => array(
			array(
				'id'   => 'home',
				'data' => array(
					'name'        => 'Home Page',
					'description' => 'Home page template.',
					'header'      => array(
						'header',
					),
					'content'     => array(
						'home-row-one',
						'home-row-two',
						'home-row-three',
					),
					'footer'      => array(
						'footer'
					),
					'sidebar_row'   => 'home-row-two',
				)
			),
			array(
				'id'   => 'post-page',
				'data' => array(
					'name'        => 'Posts & Pages',
					'description' => 'Post and page template.',
					'header'      => array(
						'header',
					),
					'content'     => array(
						'content-sidebar'
					),
					'footer'      => array(
						'footer'
					),
					'sidebar_row' => 'content-sidebar',
					'sidebar_col' => 'right'
				)
			),
			array(
				'id'   => 'content-only',
				'data' => array(
					'name'        => 'Content Only',
					'description' => 'Full width content area with no header or footer.',
					'content'     => array(
						'content'
					),
					'sidebar_row' => 'content'
				)
			),
		),
		'assignments' => array(
			'required' => array(
				'post'       => 'post-page',
				'page'       => 'post-page',
				'archive'    => 'post-page',
				'posts_page' => 'post-page',
				'front_page' => 'home',
				'bf_employees' => 'content-only'
			),
			'page'     => array(),
			'sidebars' => array()
		)
	);
}

/**
 * @return array
 */
function get_defaults_template() {
	return array(
		'id'   => 'template-' . time(),
		'data' => array(
			'name'        => '',
			'description' => '',
			'header'      => array(),
			'content'     => array(),
			'footer'      => array(),
			'sidebar_row' => '',
			'sidebar_col' => '',
			'fixed_row'   => '',
		)
	);
}

/**
 * @return array
 */
function get_defaults_row() {
	return array(
		'id'   => 'row-' . time(),
		'data' => array(
			'name'                => '',
			'description'         => '',
			'usage'               => 'content',
			'left'                => '12',
			'right'               => '0',
			'background-behavior' => 'layered',
			'animation'           => '',
			'slideshowSpeed'      => '',
			'animationSpeed'      => '',
			'min_height'          => '',
			'gutter'              => 'gutter-none',
			'custom-classes'      => array(),
			'desktop-classes'     => array(),
			'mobile-classes'      => array(),
			'background-layers'   => array(),
			// Container settings
			'container-background-behavior' => 'layered',
			'container-animation'           => '',
			'container-slideshowSpeed'      => '',
			'container-animationSpeed'      => '',
			'container-classes'     => array(),
			'container-background-layers'   => array(),
		)
	);
}

function widget_area_types() {

	/**
	 * The main key in this array is the option key which it's items are saved.
	 * The other key => value pairs are used for admin purposes.
	 */
	$types = array(
		'sidebars' => array(
			'singular_label' => 'Sidebar',
			'plural_label' => 'Sidebars',
			'add_item_label' => 'Add Sidebar',
		),
		'menu_panels' => array(
			'singular_label' => 'Menu Panel',
			'plural_label' => 'Menu Panels',
			'add_item_label' => 'Add Menu Panel',
		),
		'slides' => array(
			'singular_label' => 'Slidebar&trade;',
			'plural_label' => 'Slidebars&trade;',
			'add_item_label' => 'Add Slidebar&trade;',
		)
	);

	return $types;
}

/**
 * The groups of objects that are required to have a layout template
 *
 * @return array
 */
function required_assignment_types() {

	$types = array(
		'post' => 'Posts',
		'page' => 'Pages',
		'archive' => 'Archives',
		'posts_page' => 'WP post_page',
		'front_page' => 'WP front_page',
		'bf_employees' => 'BF Employees Single', // Custom Post Type
		'bf_location' => 'BF Locations Single', // Custom Post Type
	);

	return $types;
}