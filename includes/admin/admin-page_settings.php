<?php

/**
 * Layouts overview
 */

namespace BrightFire\Theme\Stellar;


function templates_admin() {

	global $stellar_layout;

	scripts();

	// Security
	wp_nonce_field( 'layouts_ajax_nonce', 'security' );

	$templates = $stellar_layout->get_layout_option( 'templates' );

	echo message_after_redirect(); ?>

	<h2>
		<a href="<? echo admin_url( 'admin.php?page=stellar-template-edit' ); ?>" class="page-title-action">Add
			Template</a>
	</h2>
	<table
		id="templates-table"
		class="widefat striped fixed"
		cellspacing="0">
		<thead>
		<tr>
			<th scope="col" class="column-name"><?php _e( 'Name', 'bf_stellar' ); ?></th>
			<th scope="col" class="column-defaults"><?php _e( 'Assignments', 'bf_stellar' ); ?></th>
			<th scope="col" class="column-section"><?php _e( 'Rows', 'bf_stellar' ); ?></th>
		</tr>
		</thead>

		<tbody>
		<?php

		if ( $templates ) {

			foreach ( $templates as $template_id => $template_data ) {

				?>
				<tr>
					<td>
						<a href="<? echo admin_url( 'admin.php?page=stellar-template-edit&edit=' . $template_id ); ?>"><?php echo stripslashes( $template_data[ 'name' ] ); ?></a>
						<p><?php echo $template_data[ 'description' ]; ?></p>
					</td>
					<td><?php echo template_assignments_list( $template_id ) ?></td>
					<td><?php echo row_thumbnail( $template_id, 'template' ); ?></td>

				</tr> <?php
			}
		} ?>
		</tbody>
	</table>

	<?php
}

function rows_admin() {

	global $stellar_layout;

	scripts();

	// Security
	wp_nonce_field( 'layouts_ajax_nonce', 'security' );

	$rows = $stellar_layout->get_layout_option( 'rows' );

	echo message_after_redirect(); ?>

	<h2>
		<a href="<? echo admin_url( 'admin.php?page=stellar-row-edit' ); ?>" class="page-title-action">Add Row</a>
	</h2>
	<table
		id="rows"
		class="widefat striped fixed sort-items"
		cellspacing="0">
		<thead>
		<tr>
			<th scope="col" class="column-name"><?php _e( 'Name', 'bf_stellar' ); ?></th>
			<th scope="col" class="column-description"><?php _e( 'Description', 'bf_stellar' ); ?></th>
			<th scope="col" class="column-templates"><?php _e( 'Templates', 'bf_stellar' ); ?>
		</tr>
		</thead>

		<tbody>
		<?php

		if ( $rows ) {

			foreach ( $rows as $row_id => $row_data ) {

				?>
				<tr id="<?php echo $row_id; ?>">
					<td>
						<?php echo row_thumbnail( $row_id ); ?>
					</td>
					<td>
						<p><?php echo stripslashes( $row_data[ 'description' ] ); ?></p>
					</td>
					<td>
						<?php

						$template_output = array();

						foreach ( $stellar_layout->get_associated_ids( $row_id )[ 'templates' ] as $template_id ) {
							$template = $stellar_layout->get_layout_option( 'templates' )[ $template_id ];

							$template_output[] = '<a href="' . admin_url() . '">' . $template[ 'name' ] . '</a>';
						}

						if ( empty( $template_output ) ) {
							$template_output = array( '<strong>' . __( 'UNASSIGNED', 'bf_stellar' ) . '</strong>' );
						}

						echo implode( '<br>', $template_output );
						?>
					</td>
				</tr> <?php
			}
		} ?>
		</tbody>
	</table>

	<?php
}

function widget_areas_admin() {

	global $stellar_layout;

	// Security
	wp_nonce_field( 'layouts_ajax_nonce', 'security' );

	$output = '';

	foreach ( widget_area_types() as $option_id => $labels ) {

		$option = $stellar_layout->get_layout_option( $option_id );

		$output .= "<h2>{$labels[ 'plural_label' ]}</h2>";
		$output .= "<table id='{$option_id}' class='widefat striped fixed sort-items' cellspacing='0'>";
		$output .= "<thead><tr><th scope='col' class='column-name'>Name</th><th scope='col' class='column-description'>Description</th><th scope='col' class='column-update'><button class='button add-widget-area' data-option='{$option_id}'>{$labels[ 'add_item_label' ]}</button></th></tr></thead><tbody>";
		$output .= widget_area_repeater( $option, $option_id );
		$output .= "</tbody></table>";
	}

	echo $output;
}

function mixes_admin() {
	wp_nonce_field( 'mixes_ajax_nonce', 'security' );
	$myMixes = get_option( 'stellar_mixes' );

	$mixes = array();

	if ( $myMixes ) {
		foreach ( $myMixes as $index => $values ) {
			$values[ 'required' ]           = check_mix_requirement( $index );
			$mixes[ $index ][ 'title_bar' ] = mixes_title_cb();
			$mixes[ $index ][ 'fields' ]    = mixes_fields_cb( $index, $values );
		}
	}

	// Configure Repeater Field
	$repeater = array(
		'mixes' => array( // the field id serves as the JS object namespace for each repeater
			'type'      => 'repeater',
			'instance'  => $mixes,
			'title_cb'  => array(
				'function' => '\BrightFire\Theme\Stellar\mixes_title_cb',
				'params'   => array(),
			),
			'fields_cb' => array(
				'function' => '\BrightFire\Theme\Stellar\mixes_fields_cb',
				'params'   => array(),
				'defaults' => array(),
			),
			'js_cb'     => array( 'init_selectize', 'mix_requirement' ),
			'collapse'  => true,
			'sortable'  => false,
			'add_label' => 'Add New Mix',
			'del_label' => 'Remove Mix',
			'sanitize'  => 'bypass',
			'atts'      => array(
				'data-type' => 'mixes_repeater'
			)
		)
	);

	// Build our widget form
	$args            = array(
		'fields'   => $repeater,
		'display'  => 'basic',
		'echo'     => false,
		'instance' => $mixes
	);
	$repeater_markup = \BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	$output = '';

	$output .= "<h2>Mixes</h2>";
	$output .= '<p>Mixes assigned to Widgets may not be deleted.</p>';
	$output .= $repeater_markup;
	$output .= '<button id="save-mixes" class="button-primary">Save Mixes</button>';

	echo $output;
}

function recipes_admin() {

	// Security
	wp_nonce_field( 'layouts_ajax_nonce', 'security' );
	?>
	<div id="message"></div>
	<!-- LOAD -->
	<h3>Load recipe</h3>
	<p>
		<?php echo recipe_selector( 'load-recipe-selector' ); ?>
		<label for="image-upload">
			<input type="checkbox" name="image-upload" id="image-upload" value="1"/> Import images?
		</label>
		<button class="button-primary" id="load-recipe">Load Recipe</button>
		<span id="loading" class="spinner"></span>
	</p>
	<hr>

	<!-- SAVE -->
	<h3>Save recipe</h3>
	<p>
		<input id="new-recipe-name" placeholder="Recipe name" type="text" value=""/>
		<?php if ( can_crud_network() ) { ?>
			<label for="network-recipe">
				<input type="checkbox" name="network-recipe" id="network-recipe" value="1"/> Save to the Network?
			</label>
		<?php } ?>
		<button id="save-recipe" class="button-primary">Save Recipe</button>
		<span id="saving" class="spinner"></span>
	</p>
	<hr>

	<!-- DELETE -->
	<h3>Delete Recipe</h3>
	<p>
		<?php echo recipe_selector( 'delete-recipe-selector' ); ?>
		<button class="button" id="delete-site-recipe">Delete Recipe</button>
		<span id="deleting" class="spinner"></span>
	</p>
	<hr>

	<!-- EXPORT -->
	<h3>Export recipe</h3>
	<form method="post" id="export-recipe-form" target="_blank">
		<p>
			<input type="hidden" name="export-recipe-scope" id="export-recipe-scope" value="network">
			<?php echo recipe_selector( 'export-recipe-selector' ); ?>
			<input type="submit" class="button-primary" value="Download Recipe File">
		</p>
	</form>
	<hr>

	<!-- IMPORT -->
	<h3>Import recipe</h3>
	<form method="post" id="import-recipe-form" enctype="multipart/form-data">
		<p>
			<label for="image-upload-import">
				<input type="checkbox" name="image-upload-import" id="image-upload-import" value="1"/> Import images?
			</label>
			<label class="button-primary" style="display: inline-block;">
				<input type="file" name="import-recipe-file" id="import-recipe-file"
				       style="position: fixed; top: -1000px; visibility: hidden;">
				<span>Choose Recipe File</span>
			</label>
			<span id="importing" class="spinner"></span>
		</p>
	</form>

	<?php
}

/**
 * Stellar Admin Report; Checks static page settings
 */
function stellar_admin_report() {

	echo message_after_redirect();

	// Set our inactive widgets for a cleaner site DB
	retrieve_widgets();

	/**
	 * Globals we can grab for good stuff
	 */
	global $stellar_layout, $sidebars_widgets, $wp_registered_widgets;

	$unassigned_templates = $unassigned_rows = $unassigned_sidebars = $inactive_widgets = '';


	// Defaults and Setup
	$error_items  = '';
	$found_issues = false;
	$open         = '<div class="bf-status warning"><h2><span class="fa fa-exclamation-triangle"></span> Houston, we have a problem.</h2>';
	$open .= '<ul>';
	$close = '</ul></div>';

	// Check that Front Page is set as a static page
	$front_page_id = get_option( 'page_on_front' );
	if ( ! $front_page_id ) {
		$found_issues = true;
		$error_items .= '<li>Choose a static page for the <a href="' . admin_url( 'options-reading.php' ) . '">Front Page</a> option.</li>';
	}

	// Check that Posts Page is set as a static page
	$posts_page_id = get_option( 'page_for_posts' );
	if ( ! $posts_page_id ) {
		$found_issues = true;
		$error_items .= '<li>Choose a static page for the <a href="' . admin_url( 'options-reading.php' ) . '">Posts Page</a> option.</li>';
	}

	// Check Gravity Forms Output CSS Option
	$gf_output_css = get_option( 'rg_gforms_disable_css' );
	if ( ! $gf_output_css ) {
		$found_issues = true;
		$error_items .= '<li>Set Gravity Forms <a href="' . admin_url( 'admin.php?page=gf_settings' ) . '">Output CSS option</a> to <strong>No</strong>.</li>';
	}

	// Check Gravity Forms Enable HTML5 Option
	$gf_html5 = get_option( 'rg_gforms_enable_html5' );
	if ( ! $gf_html5 ) {
		$found_issues = true;
		$error_items .= '<li>Set Gravity Forms <a href="' . admin_url( 'admin.php?page=gf_settings' ) . '">Output HTLM 5 option</a> to <strong>Yes</strong></li>';
	}

	// BODY BACKGROUND
	if ( 'missing' == get_theme_mod( 'body-background-image' ) ) {
		$found_issues = true;

		$query[ 'autofocus[control]' ] = 'body-background-image';
		$control_link                  = add_query_arg( $query, admin_url( 'customize.php' ) );

		$error_items .= '<li><a href="' . $control_link . '">Body background image is missing.</a></li>';
	}

	// ROW BACKGROUNDS
	$rows = $stellar_layout->get_layout()[ 'rows' ];

	foreach ( $rows as $id => $row ) {

		// RESET missing data variables
		$url = $missing = '';

		// Grab first template this row may be assigned to
		$template    = ( ! empty( $stellar_layout->get_associated_ids( $id )[ 'templates' ] ) ) ? $stellar_layout->get_associated_ids( $id )[ 'templates' ][ 0 ] : null;
		$assignments = $stellar_layout->get_layout_option( 'assignments' );

		if ( $stellar_layout->is_required( 'templates', $template ) ) {

			// Required assignments can be considered first
			foreach ( $assignments[ 'required' ] as $requirement => $template_id ) {
				if ( $template == $template_id ) {
					switch ( $requirement ) {
						case 'front_page':
							$url = get_site_url();
							break;
						case 'posts_page':
							$url = get_permalink( get_option( 'posts_page' ) );
							break;
						case 'archive':
							$url = get_site_url( null, 'category/uncategorized/' ); // Uncategorized archive
							break;
						case 'page':
							$pages = get_posts( array( 'post_type' => 'page' ) );
							if ( ! empty( $pages ) ) {
								$url = get_permalink( $pages[ 0 ]->ID ); // First page found
							}
							break;
						case 'post':
							$posts = get_posts();
							if ( ! empty( $posts ) ) {
								$url = get_permalink( $posts[ 0 ]->ID ); // First post found
							}
							break;
						case 'bf_location':
							$locations = get_posts( array( 'post_type' => 'bf_location' ) );
							if ( ! empty( $locations ) ) {
								$url = get_permalink( $locations[ 0 ]->ID ); // First location found
							}
							break;
						case 'bf_employees':
							$employees = get_posts( array( 'post_type' => 'bf_employees' ) );
							if ( ! empty( $employees ) ) {
								$url = get_permalink( $employees[ 0 ]->ID ); // First employees found
							}
							break;
					}
				}
			}

			// Page assignments need to be checked
			foreach ( $assignments[ 'page' ] as $requirement => $template_id ) {

				if ( $template == $template_id ) {

					$url = get_permalink( get_posts( array( 'post_type' => 'page' ) )[ 0 ]->ID ); // First page found
				}
			}

			$url = urlencode( $url );
		}

		// ROW BACKGROUNDS
		array_walk_recursive( $row, function ( &$data, &$key ) use ( &$missing, &$id ) {

			if ( 'image_id' === $key && ! empty( $data ) && empty( get_post( $data ) ) ) {

				$query = array(
					'page' => 'stellar-row-edit',
					'edit' => $id,
				);

				$link = add_query_arg( $query, admin_url( 'admin.php' ) );

				$missing .= '<li><a href="' . $link . '">Background layer image is missing.</a></li>';
			}
		} );

		// ROW WIDGETS

		$columns = array( '-left', '-right' );

		foreach ( $columns as $column ) {

			if ( isset( $sidebars_widgets[ $id . $column ] ) ) {

				// Run through widgets to find images
				foreach ( $sidebars_widgets[ $id . $column ] as $widget ) {

					// First lets get the widget settings
					$setting = get_option( $wp_registered_widgets[ $widget ][ 'callback' ][ 0 ]->option_name )[ $wp_registered_widgets[ $widget ][ 'params' ][ 0 ][ 'number' ] ];
					$name    = $wp_registered_widgets[ $widget ][ 'callback' ][ 0 ]->name;

					array_walk_recursive( $setting, function ( &$data, &$key ) use ( &$stellar_layout, &$column, &$id, &$url, &$name, &$missing, &$setting ) {

						$query = array(
							'url'                => $url,
							'autofocus[section]' => 'sidebar-widgets-' . $id . $column,
						);

						$link = add_query_arg( $query, admin_url( 'customize.php' ) );

						if ( false === $stellar_layout->is_required( 'rows', $id ) ) {
							$link = admin_url( 'widgets.php' );
						}

						// Images in widgets
						if ( 'image_id' === $key && ! empty( $data ) && empty( get_post( $data ) ) ) {
							$missing .= '<li><a href="' . $link . '">' . $name . ' image is missing.</a></li>';
						}

						// Custom Logo
						if ( 'show_logo' === $key && ! empty( $data ) && empty( get_post( get_theme_mod( 'custom_logo' ) ) ) ) {
							$query = array(
								'autofocus[control]' => 'custom_logo',
							);

							$link = add_query_arg( $query, admin_url( 'customize.php' ) );
							$missing .= '<li><a href="' . $link . '">Site logo is missing.</a></li>';
						}

						// Menu Widgets
						if ( 'menu_id' === $key && ! is_nav_menu( $data ) ) {
							$missing .= '<li><a href="' . $link . '">' . $name . ' menu is missing.</a></li>';
						}

						// Form widgets
						if ( 'form_id' === $key && false === \GFFormsModel::get_form( $data ) ) {
							$missing .= '<li><a href="' . $link . '">' . $name . ' form is missing.</a></li>';
						}

						// Location Widgets
						if ( 'location_id' === $key && empty( \BrightFire\Plugin\Locations\Query\get_locations( 'all' ) ) ) {

							$missing .= '<li><a href="' . admin_url( 'edit.php?post_type=bf_location' ) . '">Please create and set location.</a></li>';
						}
					} );
				}
			}
		}

		// ERROR ITEM
		if ( ! empty( $missing ) && $stellar_layout->is_required( 'rows', $id ) ) {

			$error_items .= '<li><strong>' . $row[ 'name' ] . ' Row</strong>';

			if ( $row[ 'description' ] ) {
				$error_items .= ': <em>' . $row[ 'description' ] . '</em>';
			}

			$error_items .= '<ul class="ul-disc">' . $missing . '</ul>';

			$error_items .= '</li>';
			$found_issues = true;

			// NON ERROR
		} elseif ( false === $stellar_layout->is_required( 'rows', $id ) ) {

			$unassigned_rows .= '<li><strong>' . $row[ 'name' ] . ' Row</strong>';

			if ( $row[ 'description' ] ) {
				$unassigned_rows .= ': <em>' . $row[ 'description' ] . '</em>';
			}
			if ( $missing ) {
				$unassigned_rows .= '<ul class="ul-disc">' . $missing . '</ul>';
			}
		}
	}

	// If we have issues
	if ( $found_issues ) {

		echo $open . $error_items . $close;

	} else {

		echo '<div class="bf-status success"><h2><span class="fa fa-rocket"></span> All systems are go!</h2></div>';
	}

	$templates = $stellar_layout->get_layout_option( 'templates' );
	$sidebars  = $stellar_layout->get_layout_option( 'sidebars' );

	foreach ( $templates as $id => $data ) {
		if ( false === $stellar_layout->is_required( 'templates', $id ) ) {
			$unassigned_templates .= '<li><a href="' . admin_url( 'admin.php?page=stellar-template-edit&edit=' . $id ) . '">' . stripslashes( $data[ 'name' ] ) . '</a></li>';
		}
	}

	foreach ( $sidebars as $id => $data ) {
		if ( false === $stellar_layout->is_required( 'sidebars', $id ) ) {
			$unassigned_sidebars .= '<li><a href="' . admin_url( 'admin.php?page=stellar-settings&tab=widget_areas' ) . '">' . stripslashes( $data[ 'name' ] ) . '</a></li>';
		}
	}

	$inactive_widgets = count( $inactive_widgets );

	if ( $unassigned_templates ) {
		$unassigned_templates = '<h2>Unassigned Templates</h2><ul class="ul-disc">' . $unassigned_templates . '</ul>';
	}
	if ( $unassigned_rows ) {
		$unassigned_rows = '<h2>Unassigned Rows</h2><ul class="ul-disc">' . $unassigned_rows . '</ul>';
	}
	if ( $unassigned_sidebars ) {
		$unassigned_sidebars = '<h2>Unassigned Sidebars</h2><ul class="ul-disc">' . $unassigned_sidebars . '</ul>';
	}
	if ( isset( $sidebars_widgets[ 'wp_inactive_widgets' ] ) ) {
		$inactive_widgets = '<h2>' . count( $sidebars_widgets[ 'wp_inactive_widgets' ] ) . ' <a href="' . admin_url( 'widgets.php?#wp_inactive_widgets' ) . '"> inactive widgets</a>.</h2>';
	}

	echo $unassigned_templates . $unassigned_rows . $unassigned_sidebars . $inactive_widgets;


}

function themes_admin() {

	wp_nonce_field( 'theme_ajax_nonce', 'security' );

	$theme        = get_theme_mod( 'theme', false );
	$theme_path   = BF_STELLAR_PATH . 'themes';
	$theme_array  = directoryToArray( $theme_path );
	$theme_config = array();

	// Theme Files expected
	$expected_files = array(
		'styles.scss'    => true,
		'vars.scss'      => true,
		'functions.php'  => false,
		'screenshot.png' => false,
		'theme.md'       => false,
		'scripts.js'     => true,
	);

	// Run through each theme we found
	foreach ( $theme_array as $theme_name => $theme_files ) {

		$theme_config[ $theme_name ][ 'files' ] = $theme_files;

		if ( $theme_name == $theme ) {
			$theme_config[ $theme_name ][ 'active' ] = true;
		} else {
			$theme_config[ $theme_name ][ 'active' ] = false;
		}

		/**
		 * Check our Found Files against Expected Files to ensure we have all required files
		 */
		foreach ( $expected_files as $filename => $required ) {

			// Does the file exist?
			$filefound = array_search( $filename, $theme_files );

			// If no file, see if it was required
			if ( false === $filefound ) {
				if ( $required ) {
					// File IS Required - Theme now invalid
					$theme_config[ $theme_name ][ 'error' ][] = 'Theme [' . ucfirst( $theme_name ) . '] is missing the required file [' . $filename . '] and cannot be used';
				} else {
					// FIle NOT Required - let me people know if we don't have it
					$theme_config[ $theme_name ][ 'notice' ][] = 'Theme [' . ucfirst( $theme_name ) . '] is missing a file [' . $filename . ']. This file is not required, but can be beneficial to Theme Functions and Extensability';
				}
			}
		}

	}

	/**
	 * Loop Through our compiled array to output
	 */

	$output = '';

	foreach ( $theme_config as $theme_name => $theme_details ) {

		// If Theme has no error
		if ( ! array_key_exists( 'error', $theme_details ) ) {

			$theme_info_arr         = array();
			$theme_info_arr[ 'id' ] = $theme_name;

			if ( $theme_details[ 'active' ] ) {
				$active = ' active';
			} else {
				$active = '';
			}

			// Theme Info Array
			if ( file_exists( BF_STELLAR_PATH . 'themes/' . $theme_name . '/theme.md' ) ) {

				$theme_info = file_get_contents( BF_STELLAR_PATH . 'themes/' . $theme_name . '/theme.md' );

				if ( preg_match( '|^[ \t\/*#@]*Theme Name:(.*)$|mi', $theme_info, $info_theme_name ) ) {
					$theme_info_arr[ 'name' ] = trim( $info_theme_name[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Theme URI:(.*)$|mi', $theme_info, $info_theme_url ) ) {
					$theme_info_arr[ 'uri' ] = trim( $info_theme_url[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Description:(.*)$|mi', $theme_info, $info_theme_description ) ) {
					$theme_info_arr[ 'desc' ] = trim( $info_theme_description[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Author:(.*)$|mi', $theme_info, $info_theme_author ) ) {
					$theme_info_arr[ 'author' ] = trim( $info_theme_author[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Author URI:(.*)$|mi', $theme_info, $info_theme_author_uri ) ) {
					$theme_info_arr[ 'author_uri' ] = trim( $info_theme_author_uri[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Version:(.*)$|mi', $theme_info, $info_theme_version ) ) {
					$theme_info_arr[ 'ver' ] = trim( $info_theme_version[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Tags:(.*)$|mi', $theme_info, $info_theme_tags ) ) {
					$theme_info_arr[ 'tags' ] = trim( $info_theme_tags[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*Text Domain:(.*)$|mi', $theme_info, $info_theme_text_domain ) ) {
					$theme_info_arr[ 'text_domain' ] = trim( $info_theme_text_domain[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*License:(.*)$|mi', $theme_info, $info_theme_license ) ) {
					$theme_info_arr[ 'license' ] = trim( $info_theme_license[ 1 ] );
				}
				if ( preg_match( '|^[ \t\/*#@]*License URI:(.*)$|mi', $theme_info, $info_theme_license_uri ) ) {
					$theme_info_arr[ 'license_uri' ] = trim( $info_theme_license_uri[ 1 ] );
				}


			} else {
				$theme_info_arr[ 'name' ]       = ( '' == $theme_name ? 'BrightFire Stellar' : ucfirst( $theme_name ) );
				$theme_info_arr[ 'uri' ]        = 'http://www.brightfire.com';
				$theme_info_arr[ 'author' ]     = 'BrightFire';
				$theme_info_arr[ 'author_uri' ] = 'http://www.brightfire.com';
				$theme_info_arr[ 'ver' ]        = BF_STELLAR_VERSION;
				$theme_info_arr[ 'desc' ]       = '';
			}

			if ( file_exists( BF_STELLAR_PATH . 'themes/' . $theme_name . '/screenshot.png' ) ) {
				$theme_info_arr[ 'screenshot' ] = BF_STELLAR_URL . '/themes/' . $theme_name . '/screenshot.png';
				$screenshot                     = '<img src="' . BF_STELLAR_URL . '/themes/' . $theme_name . '/screenshot.png" alt="">';
			}

			// Overlay reads this info
			$theme_info_json = json_encode( $theme_info_arr );

			$output .= '<div class="stellar-theme' . $active . ' ' . $theme_name . '" data-info=\'' . $theme_info_json . '\'">';
			$output .= '<div class="stellar-theme-screenshot">';
			$output .= $screenshot;
			$output .= '</div>';
			$output .= '<span class="stellar-more-details">Theme Details</span>';
			$output .= '<h2 class="stellar-theme-name' . $active . '">';
			$output .= '<span class="stellar-theme-name-prefix">Active: </span>';
			$output .= '<span class="stellar-theme-name-text">' . $theme_info_arr[ 'name' ] . '</span>';
			$output .= '</h2>';
			$output .= '<div class="stellar-theme-actions' . $active . '">';
			$output .= '<div class="active-actions">';
			$output .= '<span class="spinner spinner-dark"></span> <a class="button button-primary stellar-theme-deactivate" href="javascript: void(0);">Deactivate</a>';
			$output .= '</div>';
			$output .= '<div class="inactive-actions">';
			$output .= '<span class="spinner"></span> <a class="button button-primary stellar-theme-activate" href="javascript: void(0);" data-theme="' . $theme_name . '">Activate</a>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div >';

		}

	}

	// Overlay for Theme Details
	$output .= '<div class="stellar-theme-overlay"><div class="theme-overlay active">';
	$output .= '<div class="theme-backdrop"></div>';
	$output .= '<div class="theme-wrap wp-clearfix">';
	$output .= '<div class="theme-header">';
	$output .= '<button class="close dashicons dashicons-no"><span class="screen-reader-text">Close details dialog</span></button>';
	$output .= '</div>';
	$output .= '<div class="theme-about wp-clearfix">';
	$output .= '<div class="theme-screenshots">';
	$output .= '<div class="screenshot">';
	$output .= '<img src="" alt="">';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="theme-info">';
	$output .= '<span class="current-label">Current Theme</span>';
	$output .= '<h2 class="theme-name"><span class="theme-version"></span></h2>';
	$output .= '<p class="theme-author">By <a href="https://www.brightfire.com"></a></p>';
	$output .= '<p class="theme-description"></p>';
	$output .= '<p class="parent-theme">This is a child theme of <b>BrightFire Stellar</b></p>';
	$output .= '<p class="theme-license"></p>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="theme-actions">';
	$output .= '<div class="active-actions">';
	$output .= '</div>';
	$output .= '<div class="inactive-actions">';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div></div>';


	$output .= '<div class="clear"></div>';

	echo $output;

}


/**
 * Setup Tab navigation for ready classes
 *
 * @param string $current
 *
 * @return string
 */
function documentation_tabs( $current = 'theme' ) {
	$bf_admin_tabs = array(
		'theme'            => 'Theme',
		'structure'        => 'Structure',
		'rows'             => 'Rows',
		'row_container'    => 'Row Container',
		'widget_classes'   => 'Widget Classes',
		'widget_titles'    => 'Widget Titles',
		'menu_bar_widget'  => 'Menu Bar Widget',
		'brightfire_forms' => 'BrightFire Form Widget',
		'bucket_widget'    => 'Bucket Widget',
	);

	$tabs = '';
	$tabs .= '<h2 class="nav-tab-wrapper">';

	foreach ( $bf_admin_tabs as $tab => $name ) {
		$class = ( $tab == $current ) ? ' nav-tab-active' : '';
		$tabs .= '<a class="bf-admin-api-nav-tab nav-tab' . $class . '" href="?page=stellar-documentation&tab=' . $tab . '">' . $name . '</a>';
	}

	$tabs .= '</h2>';

	return $tabs;
}

function documentation_admin( $current_tab ) {

	$widget = '';

	$structure_class_options   = apply_filters( 'stellar_widget_structure_classes', \BrightFire\Theme\Stellar\widget_structure_choices() );
	$offset_class_options      = apply_filters( 'stellar_widget_offset_classes', \BrightFire\Theme\Stellar\widget_offset_choices() );
	$custom_class_options      = apply_filters( 'stellar_widget_class_choices', \BrightFire\Theme\Stellar\widget_class_choices(), $widget );
	$custom_breakpoint_options = apply_filters( 'stellar_widget_breakpoint_choices', \BrightFire\Theme\Stellar\widget_breakpoint_choices(), $widget );
	$title_class_options       = apply_filters( 'stellar_widget_title_class_choices', \BrightFire\Theme\Stellar\widget_title_class_choices(), $widget );

	switch ( $current_tab ) {
		case 'structure':
			echo '<h3>Column Widths</h3>';
			render_ready_class_table( $structure_class_options );
			echo '<h3>Offsets</h3>';
			render_ready_class_table( $offset_class_options );
			echo '<h3>Lists</h3>';
			render_ready_class_table( list_column_choices() );
			break;
		case 'widget_classes':
			render_ready_class_table( $custom_class_options, $custom_breakpoint_options );
			break;
		case 'widget_titles':
			render_ready_class_table( $title_class_options );
			break;
		case 'menu_bar_widget':
			render_ready_class_table( menu_bar_class_choices(), menu_bar_breakpoint_choices() );
			break;
		case 'brightfire_forms':
			render_ready_class_table( brightfire_forms_class_choices(), brightfire_forms_breakpoint_choices() );
			break;
		case 'bucket_widget':
			echo '<h4>The BrightFire Bucket Widget has special classes that can be applied to individual Block types.</h4>';

			echo '<h3>Block Type: Heading</h3>';
			render_ready_class_table( bf_bucket_heading_classes(), bf_bucket_heading_breakpoint() );

			echo '<h3>Block Type: Image/Icon</h3>';
			render_ready_class_table( bf_bucket_image_classes(), bf_bucket_image_breakpoint() );

			echo '<h3>Block Type: Text</h3>';
			render_ready_class_table( bf_bucket_text_classes(), bf_bucket_text_breakpoint() );

			echo '<h3>Block Type: Button</h3>';
			render_ready_class_table( bf_bucket_button_classes(), bf_bucket_button_breakpoint() );
			break;
		case 'rows':
			render_ready_class_table( row_class_choices(), row_breakpoint_choices() );
			break;
		case 'row_container':
			render_ready_class_table( row_container_class_choices(), row_container_breakpoint_choices() );
			break;
		default :
			echo apply_filters( 'stellar_theme_documentation', '<p>These pages show the availability of classes in context to their use within the site. Stellar themes can change class availability and these pages can be used as reference.</p>' );
	}
}

function render_ready_class_table( $global = array(), $breakpoint = array() ) {

	echo '<table class="widefat striped fixed" cellspacing="0">' .
	     '<thead>' .
	     '<tr>' .
	     '<th width="10%"><b>Availability</b></th>' .
	     '<th width="20%"><b>Class</b></th>' .
	     '<th width="20%"><b>Literal</b></th>' .
	     '<th><b>Description</b></th>' .
	     '</tr>' .
	     '</thead>' .
	     '<tbody>';

	$classes = array_merge( $global, $breakpoint );

	asort( $classes );

	foreach ( $classes as $class => $info ) {

		if ( array_key_exists( $class, $global ) ) {
			$global_icon = '<i class="fa fa-globe fa-lg text-success"></i>';
		} else {
			$global_icon = '<i class="fa fa-globe fa-lg text-warning"></i>';
		}

		if ( array_key_exists( $class, $breakpoint ) && ! empty( $breakpoint ) ) {
			$breakpoint_icon = ' <i class="fa fa-laptop fa-lg text-success"></i>';
			$breakpoint_icon .= ' <i class="fa fa-mobile fa-lg text-success"></i>';

		} elseif ( ! empty( $breakpoint ) ) {
			$breakpoint_icon = ' <i class="fa fa-laptop fa-lg text-warning"></i>';
			$breakpoint_icon .= ' <i class="fa fa-mobile fa-lg text-warning"></i>';
		} else {
			$breakpoint_icon = '';
		}

		echo '<tr>';
		echo '<td>';
		echo $global_icon . $breakpoint_icon;
		echo '</td>';
		echo '<td>';
		echo '<b>' . ( isset( $info[ 'name' ] ) ? $info[ 'name' ] : '' ) . '</b>';
		echo '</td>';
		echo '<td>';
		echo '<i>.' . $class . '</i>';
		echo '</td>';
		echo '<td>';
		echo '<i>' . ( isset( $info[ 'desc' ] ) ? $info[ 'desc' ] : 'No Description Available' ) . '</i>';
		echo '</td>';
		echo '</tr>';

	}

	echo '</tbody>' .
	     '</table>';

}

/**
 * Scripts admin page
 */
function scripts_admin() {

	scripts();

	// Security
	wp_nonce_field( 'stellar-scripts', 'security' );

	$scripts = get_option( 'stellar_scripts' );

	// Escape our data
	if ( $scripts ) {
		$scripts[ 'header_scripts' ] = stripslashes( $scripts[ 'header_scripts' ] );
		$scripts[ 'footer_scripts' ] = stripslashes( $scripts[ 'footer_scripts' ] );
	}

	\BF_Admin_API_Fields::bf_admin_api_fields_build(
		array(
			'fields' => array(
				'header_scripts' => array(
					'label'       => 'Header Scripts',
					'description' => 'Scripts that output in the <code>head</code> tag of every page.',
					'type'        => 'textarea',
					'default_value'     => '',
					'permit'      => 1,
					'sanitize' => 'bypass',
				),
				'footer_scripts' => array(
					'label'       => 'Footer Scripts',
					'description' => 'Scripts that output just before the closing <code>body</code> tag of every page',
					'type'        => 'textarea',
					'default_value'     => '',
					'permit'      => 1,
					'sanitize' => 'bypass',
				),
			),
			'instance' => $scripts,
			'display' => 'basic',
			'section_id' => 'stellar_scripts',
			'echo' => true,
		)
	);

	// Save Button
	echo '<p><button id="save-scripts" class="button-primary">Save Scripts</button><p>';
}