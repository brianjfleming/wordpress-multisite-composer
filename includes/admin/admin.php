<?php

namespace BrightFire\Theme\Stellar;

/**
 * Admin Menus
 */
function stellar_admin_menus() {

	/** Main stellar menu */
	add_menu_page( 'Stellar', 'Stellar', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings', __NAMESPACE__ . '\stellar_settings', 'dashicons-marker', 58 );
	add_submenu_page( null, 'New Template', 'New Template', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-template-edit', __NAMESPACE__ . '\layouts_template_edit' );
	add_submenu_page( null, 'New Row', 'New Row', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-row-edit', __NAMESPACE__ . '\layouts_row_edit' );
	add_submenu_page( 'stellar-settings', 'Scripts', 'Scripts', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=scripts', __NAMESPACE__ . '\scripts_admin' );
	add_submenu_page( 'stellar-settings', 'Templates', 'Templates', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=templates', __NAMESPACE__ . '\templates_admin' );
	add_submenu_page( 'stellar-settings', 'Rows', 'Rows', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=rows', __NAMESPACE__ . '\rows_admin' );
	add_submenu_page( 'stellar-settings', 'Widget Areas', 'Widget Areas', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=widget_areas', __NAMESPACE__ . '\widget_areas_admin' );
	add_submenu_page( 'stellar-settings', 'Mixes', 'Mixes', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=mixes', __NAMESPACE__ . '\mixes_admin' );
	add_submenu_page( 'stellar-settings', 'Recipes', 'Recipes', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=recipes', __NAMESPACE__ . '\recipes_admin' );
	add_submenu_page( 'stellar-settings', 'Themes', 'Themes', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-settings&tab=themes', __NAMESPACE__ . '\themes_admin' );
	add_submenu_page( 'stellar-settings', 'Documentation', 'Documentation', BF_STELLAR_LAYOUTS_CAPABILITY, 'stellar-documentation', __NAMESPACE__ . '\stellar_documentation_admin' );

	// Highlight Sub Menu Tabbed pages
	add_filter( 'parent_file', function ( $parent_file ) {
		global $submenu_file;
		if ( isset( $_GET[ 'tab' ] ) ) {
			$submenu_file = $_GET[ 'page' ] . '&tab=' . $_GET[ 'tab' ];
		}

		return $parent_file;
	} );

}

/**
 * WP Admin Bar
 */
function stellar_admin_bar() {
	global $wp_admin_bar;

	if ( ! current_user_can( BF_STELLAR_LAYOUTS_CAPABILITY ) ) {
		return false;
	}

	$stellar_icon = '<span class="dashicons dashicons-marker" style="font-family: \'dashicons\'"></span> ';

	// Main Stellar Link
	$wp_admin_bar->add_menu( array(
		'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar', // link ID, defaults to a sanitized title value
		'title'  => $stellar_icon . __( ' Stellar' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Scripts
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_scripts', // link ID, defaults to a sanitized title value
		'title'  => __( 'Scripts' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=scripts' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Templates
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_templates', // link ID, defaults to a sanitized title value
		'title'  => __( 'Templates' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=templates' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Rows
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_rows', // link ID, defaults to a sanitized title value
		'title'  => __( 'Rows' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=rows' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Widget Areas
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_widget_areas', // link ID, defaults to a sanitized title value
		'title'  => __( 'Widget Areas' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=widget_areas' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Mixes
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_mixes', // link ID, defaults to a sanitized title value
		'title'  => __( 'Mixes' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=mixes' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Recipes
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_recipes', // link ID, defaults to a sanitized title value
		'title'  => __( 'Recipes' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=recipes' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Themes
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_themes', // link ID, defaults to a sanitized title value
		'title'  => __( 'Themes' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-settings&tab=themes' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	// Ready Classes Documentations
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_documentation', // link ID, defaults to a sanitized title value
		'title'  => __( 'Documentation' ), // link title
		'href'   => admin_url( 'admin.php?page=stellar-documentation' ), // name of file
		'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );

	$trumpisms = array(
		'Make Stellar Great Again',
		'Build That CSS',
		'Drain The SASS',
		'C. S. S. !!!',
		'CSS For Prison',
		'Trump That SASS',
		'Grab `em By The Sassy',
		'Total Control, bing, bing, bong, bong, bingbingbing.'
	);

	// Ready Classes Documentations
	$wp_admin_bar->add_menu( array(
		'parent' => 'stellar', // use 'false' for a root menu, or pass the ID of the parent menu
		'id'     => 'stellar_delete_css', // link ID, defaults to a sanitized title value
		'title'  => $trumpisms[ mt_rand( 0, count( $trumpisms ) - 1 ) ], // link title
		'href'   => '#', // name of file
		'meta'   => array( 'onclick' => 'deleteStellarCSS()' ) // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
	) );
}

/**
 * Setup Tab pages
 */
function stellar_settings() {

	// Check Capabilities
	if ( ! current_user_can( BF_STELLAR_LAYOUTS_CAPABILITY ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	// What tab are we on now?
	$current_tab = ( isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : '' );

	// Setup our tabs
	echo '<div class="wrap">';
	echo '<h2>Stellar Settings</h2>';
	echo stellar_settings_tabs( $current_tab );

	echo '<div class="tab-wrapper">';
	echo '<div class="inside">';

	switch ( $current_tab ) {
		case 'scripts' :
			scripts_admin();
			break;
		case 'templates' :
			templates_admin();
			break;
		case 'rows' :
			rows_admin();
			break;
		case 'widget_areas' :
			widget_areas_admin();
			break;
		case 'mixes' :
			mixes_admin();
			break;
		case 'recipes' :
			recipes_admin();
			break;
		case 'themes' :
			themes_admin();
			break;
		default :
			stellar_admin_report();
	}

	echo '</div>';
	echo '</div>';
	echo '</div>';

}

function stellar_documentation_admin() {

	// Check Capabilities
	if ( ! current_user_can( BF_STELLAR_LAYOUTS_CAPABILITY ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	// What tab are we on now?
	$current_tab = ( isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'theme' );

	// Setup our tabs
	echo '<div class="wrap">';
	echo '<h2>Stellar Documentation</h2>';
	echo documentation_tabs( $current_tab );

	echo '<div class="tab-wrapper">';
	echo '<div class="inside">';

	documentation_admin( $current_tab );

	echo '</div>';
	echo '</div>';
	echo '</div>';
}

/**
 * Setup Tab navigation
 *
 * @param string $current
 *
 * @return string
 */
function stellar_settings_tabs( $current = '' ) {
	$bf_admin_tabs = array(
		''             => '<i class="fa fa-question-circle"></i> Status',
		'scripts'      => '<i class="fa fa-code"></i> Scripts',
		'templates'    => '<i class="fa fa-newspaper-o"></i> Templates',
		'rows'         => '<i class="fa fa-bars"></i> Rows',
		'widget_areas' => '<i class="fa fa-columns"></i> Widget Areas',
		'mixes'        => '<i class="fa fa-spoon"></i> Mixes',
		'recipes'      => '<i class="fa fa-book"></i> Recipes',
		'themes'       => '<i class="fa fa-briefcase"></i> Themes',
	);

	$tabs = '';
	$tabs .= '<h2 class="nav-tab-wrapper">';

	foreach ( $bf_admin_tabs as $tab => $name ) {
		$class = ( $tab == $current ) ? ' nav-tab-active' : '';
		$tabs .= '<a class="bf-admin-api-nav-tab nav-tab' . $class . '" href="?page=stellar-settings&tab=' . $tab . '">' . $name . '</a>';
	}

	$tabs .= '</h2>';

	return $tabs;
}

/**
 * Enqueue script helper
 */
function enqueue_admin_scripts() {

	wp_register_script( 'stellar-nav-menu', BF_STELLAR_URL . '/assets/js/src/stellar-nav-menu.js' );
	wp_register_script( 'stellar-admin-scripts', BF_STELLAR_URL . '/assets/js/admin.js', array( 'jquery', 'jquery-ui-slider', 'jquery-ui-sortable', 'wp-color-picker' ), BF_STELLAR_VERSION );
	wp_register_style( 'stellar-admin-styles', BF_STELLAR_URL . '/assets/css/admin.css' );
	admin_bar_scripts();

	$screen = get_current_screen();

	$screens = array(
		'widgets',
		'customize',
		'nav-menus',
		'edit',
		'toplevel_page_stellar-settings',
		'admin_page_stellar-template-edit',
		'admin_page_stellar-row-edit',
		'stellar_page_stellar-documentation'
	);

	// roll up our admin scripts for use on specific screens
	if ( in_array( $screen->base, $screens ) ) {
		wp_enqueue_script( 'jquery-ui-sortable', 'jquery' );
		wp_enqueue_script( 'jquery-ui-slider', 'jquery' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_media();
		wp_enqueue_script( 'stellar-admin-scripts' );
		wp_enqueue_script( 'stellar-nav-menu', 'jquery' );
		wp_enqueue_style( 'stellar-admin-styles' );
		wp_enqueue_style( 'fontawesome' );
		wp_enqueue_style(
			'bf_stellar_site',
			site_stellar_css_url( true ),
			array(),
			time()
		);
	}
}

/**
 * Javascript needed for our delete CSS ajax
 */
function admin_bar_scripts() {

	if ( is_admin_bar_showing() ) {
		wp_enqueue_script( 'delete_stellar_css', BF_STELLAR_URL . '/assets/js/src/delete-stellar-css.js', array( 'jquery' ) );
		wp_localize_script( 'delete_stellar_css', 'resetCSS', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'security' => wp_create_nonce( 'delete-stellar-css' ) ) );
	}

}

/**
 * Remove our Page Attributes MetaBox from whichever page is set to our homepage
 */
function remove_template_metabox() {

	// Get our homepage id
	$front_page_id = get_option( 'page_on_front' );
	if ( isset( $_GET[ 'post' ] ) && $_GET[ 'post' ] == $front_page_id ) {
		remove_meta_box( 'pageparentdiv', 'page', 'normal' );
	}

}

/**
 * Checks if a mix is assigned to any widgets
 *
 * @param $mix_id
 *
 * @return string Returns "true" if found and empty if not
 */
function check_mix_requirement( $mix_id ) {
	global $stellar_layout, $sidebars_widgets, $wp_registered_widgets;

	$rows = $stellar_layout->get_layout()[ 'rows' ];

	$return = '';

	foreach ( $rows as $id => $row ) {

		$columns = array( '-left', '-right' );

		foreach ( $columns as $column ) {

			if ( isset( $sidebars_widgets[ $id . $column ] ) ) {

				foreach ( $sidebars_widgets[ $id . $column ] as $widget ) {

					$setting = get_option( $wp_registered_widgets[ $widget ][ 'callback' ][ 0 ]->option_name )[ $wp_registered_widgets[ $widget ][ 'params' ][ 0 ][ 'number' ] ];

					array_walk_recursive( $setting, function ( &$data, &$key ) use ( &$mix_id, &$return ) {

						if ( $mix_id === $data ) {
							$return = 'true'; // We found the mix being used in a widget
						}
					} );
				}
			}
		}
	}

	return $return;
}

/**
 * Add Post Form ID Metabox for pages
 */
function post_form_metabox() {
	add_meta_box( 'post_form_meta_box', __( 'Page Form Assignment', 'bf_stellar' ), __NAMESPACE__ . '\post_form_meta_box', 'page', 'side' );
}

/**
 * Callback for Post Form ID Metabox
 *
 * @param string $post
 */
function post_form_meta_box( $post = '' ) {

	wp_nonce_field( 'post_form_meta_box', 'post_form_meta_box_nonce' );

	$current = array(
		'post_form_id' => get_post_meta( $post->ID, 'post_form_id', true )
	);

	$fields = array(
		'post_form_id' => array(
			'type'          => 'selectize',
			'description'   => '<p>Assign a form to this page that can be used with the BrightFire Form Widget when "Dynamic" is selected</p>',
			'choices'       => get_gf_forms(),
			'default_value' => 0,
		)
	);

	$args = array(
		'fields'   => $fields,
		'instance' => $current,
		'display'  => 'basic',
		'echo'     => true,
	);

	\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

}

/**
 * Save hook for post_meta 'post_form_id'
 *
 * @param $post_id
 * @param $post
 */
function save_post_force_ssl( $post_id, $post ) {

	// Check if our nonce is set.
	if ( ! isset( $_REQUEST[ 'post_form_meta_box_nonce' ] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_REQUEST[ 'post_form_meta_box_nonce' ], 'post_form_meta_box' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( ! current_user_can( BF_STELLAR_LAYOUTS_CAPABILITY, $post_id ) ) {
		return;
	}

	/* OK, it's safe for us to save the data now. */

	if ( isset( $_REQUEST[ 'post_form_id' ] ) ) {
		$form_id = $_REQUEST[ 'post_form_id' ];
	} else {
		$form_id = '0'; // For posterity
	}

	update_post_meta( $post_id, 'post_form_id', $form_id );
}