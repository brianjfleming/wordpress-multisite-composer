<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 7/7/15
 * Time: 4:06 PM
 */

if( class_exists( 'WP_Customize_Control' ) ) {
	/**
	 * Multiple checkbox customize control class.
	 *
	 * @since  1.0.0
	 * @access public
	 */
	class BrightFire_Control_Theme_Text extends WP_Customize_Control {

		/**
		 * The type of customize control being rendered.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $type = 'theme-text';

		/**
		 * Displays the control content.
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function render_content() {

			$output = '';

			// Get our current theme
			$theme      = get_theme_mod( 'theme', false );
			$theme_name = '<i>Stellar Defaults</i>';

			if ( ! empty( $this->label ) ) {
				$output .= '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';
			}

			if ( ! empty( $this->description ) ) {
				$output .= '<span class="description customize-control-description">' . $this->description . '</span>';
			}

			if ( 'default' != $theme && '' != $theme ) {

				// Read our theme settings / files
				if ( file_exists( BF_STELLAR_PATH . 'themes/' . $theme . '/theme.md' ) ) {
					$theme_name = '<i>' . ucfirst( $theme ) . '</i>';
					$theme_info = file_get_contents( BF_STELLAR_PATH . 'themes/' . $theme . '/theme.md' );

					if ( preg_match( '|^[ \t\/*#@]*Theme Name:(.*)$|mi', $theme_info, $info_theme_name ) ) {
						$theme_name = trim( $info_theme_name[1] );
					}
				}

			}

			$output .= $theme_name . ' | <a href="' . admin_url() . 'admin.php?page=stellar-settings&tab=themes">Change</a>';

			echo $output;

		}


	}
	
}
