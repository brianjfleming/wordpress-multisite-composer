<?php

class BrightFire_Theme_Stellar_Customizer {

	public static $prefix = 'bf_theme_stellar_';
	protected $panel_name = 'brightfire_stellar_settings';
	protected $panel_title = 'Stellar';
	protected $sections_fields;
	protected $section_name = '';
	protected $section_title = '';
	
	function __construct( $section_name = false, $section_title = '', $sections_fields = array() ) {
		$this->section_name = $section_name;
		$this->section_title = $section_title;
		$this->sections_fields = $sections_fields;
		add_action('customize_register', array( $this ,'bf_customize_register_settings' ) );
	}

	public function bf_customize_register_settings( $wp_customize ) {
		
		// Build the panel
		$wp_customize->add_panel( $this->panel_name , array(
			'title' => $this->panel_title,
			'capability' => 'edit_pages',
			'description' => '', // Include html tags such as <p>.
			'priority' => 30, // Mixed with top-level-section hierarchy.
		) );

		// Build the sections and the options/fields nested in the array
		foreach( $this->sections_fields as $section => $section_values ) {
			$wp_customize->add_section( $this->section_name , array(
				'title' => $this->section_title,
				'capability' => $section_values['capability'],
				'panel' => $this->panel_name,
			) );


			foreach ( $section_values['fields'] as $option_name => $option_values ) {

				$this->create_setting( $wp_customize, $option_name, $option_values );

				if ( 'checkboxes' == $option_values['type'] ) {
					$this->create_checkboxes_control( $wp_customize, $section, $option_name, $option_values );
				} elseif ( 'image' == $option_values['type'] ) {
					$this->create_image_upload_control( $wp_customize, $section, $option_name, $option_values );
				} elseif ( 'selectize' == $option_values['type'] ) {
					$this->create_selectize_control( $wp_customize, $section, $option_name, $option_values );
				} elseif ( 'media' == $option_values['type'] ) {
					$this->create_media_control( $wp_customize, $section, $option_name, $option_values );
				} elseif ( 'color_picker' == $option_values['type'] ) {
					$this->create_color_picker_control( $wp_customize, $section, $option_name, $option_values );
				} elseif ( 'theme_text' == $option_values['type'] ) {
					$this->create_bf_theme_text_control( $wp_customize, $section, $option_name, $option_values );
				} else {
					$this->create_standard_control( $wp_customize, $section, $option_name, $option_values );
				}

			} // /field build

		} // /section build

	}

	protected function create_setting( $wp_customize, $option_name, $option_values ) {

		$capability = ( 1 === $option_values['permit'] ) ? 'edit_pages' : 'edit_theme_options';
		$default = ( empty( $option_values['default'] ) ) ? '' : $option_values['default'];
		$transport = ( empty( $option_values['transport'] ) ) ? 'refresh' : $option_values['transport'];
		$option_type = ( empty( $option_values[ 'option_type' ] ) ) ? 'theme_mod' : $option_values[ 'option_type' ];


		$wp_customize->add_setting( $option_name, array(
			'type' => $option_type, // or 'option'
			'capability' => $capability, //'edit_theme_options',
			'theme_supports' => '', // Rarely needed.
			'default' => $default,
			'transport' => $transport, // or postMessage
			'sanitize_callback' => $this->get_sanitize_callback( $option_values['type'] ),

		) );
	}

	protected function create_bf_theme_text_control( $wp_customize, $section, $option_name, $option_values ) {

		$wp_customize->add_control( new BrightFire_Control_Theme_Text(
			$wp_customize,
			$option_name,
			array(
				'label' => $option_values['label'],
				'description' => $option_values['description'],
				'section' => $section
			)
		) );

	}

	protected function create_standard_control( $wp_customize, $section, $option_name, $option_values ) {

		$placeholder = empty( $option_values['placeholder'] ) ? $option_values['label'] : $option_values['placeholder'] ;
		$choices = empty( $option_values['choices'] ) ? array() : $option_values['choices'] ;
		$input_attrs = empty( $option_values['input_attrs'] ) ? array ( 'placeholder' => $placeholder ) : $option_values['input_attrs'];
		$settings = array_key_exists( 'settings', $option_values ) ? $option_values['settings'] : $option_name ;

		$wp_customize->add_control( $option_name , array(
			'label' => $option_values['label'],
			'description' => $option_values['description'],
			'type' =>  $option_values['type'],
			'section' => $section,
			'choices' => $choices,
			'input_attrs' => $input_attrs,
			'settings' => $settings,
		) );
	}

	protected function create_checkboxes_control( $wp_customize, $section, $option_name, $option_values ) {

		$placeholder = empty( $option_values['placeholder'] ) ? $option_values['label'] : $option_values['placeholder'] ;

		$wp_customize->add_control( new BrightFire_Control_Checkboxes(
			$wp_customize,
			$option_name,
			array(
				'label' => $option_values['label'],
				'description' => $option_values['description'],
				'section' => $section,
				'choices' => $option_values['choices'],
				'input_attrs' => array (
					'placeholder' => $placeholder,
				)
			)

		) );
	}

	protected function create_image_upload_control( $wp_customize, $section, $option_name, $option_values ) {

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
			// $wp_customize object
				$wp_customize,
				$option_name,
				// $args
				array(
					'label' => $option_values['label'],
					'description' => $option_values['description'],
					'section' => $section,
				)
			)
		);

	}

	protected function create_media_control( $wp_customize, $section, $option_name, $option_values ) {

		$wp_customize->add_control(
			new WP_Customize_Media_Control(
			// $wp_customize object
				$wp_customize,
				$option_name,
				// $args
				array(
					'label' => $option_values['label'],
					'description' => $option_values['description'],
					'section' => $section,
				)
			)
		);

	}

	protected function create_color_picker_control( $wp_customize, $section, $option_name, $option_values ) {

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
			// $wp_customize object
				$wp_customize,
				$option_name,
				// $args
				array(
					'label' => $option_values['label'],
					'description' => $option_values['description'],
					'section' => $section,
				)
			)
		);
	}

	protected function create_selectize_control( $wp_customize, $section, $option_name, $option_values ) {

		$wp_customize->add_control( new BrightFire_Control_Selectize(
			$wp_customize,
			$option_name,
			array(
				'label' => $option_values['label'],
				'description' => $option_values['description'],
				'section' => $section,
				'choices' => $option_values['choices'],
				'input_attrs' => $option_values['input_attrs']
				)
			)
		);
	}
	
	protected function get_sanitize_callback( $setting_type ) {
		switch ( $setting_type ) {
			case 'email' : return 'sanitize_email';
			case 'url' : return 'esc_url_raw';
			case 'checkboxes' : return 'bf_sanitize_checkbox_array';
			case 'selectize' : return 'bf_sanitize_checkbox_array';

			default : return 'sanitize_text_field';
		}
	}

}
