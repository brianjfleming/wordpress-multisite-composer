<?php

/**
 * Class Stellar_Nav_Menu_Item_Custom_Fields
 * 
 * Main Stellar Admin Class for menus. Handles setup, hooks save,
 * and generates our menu items
 * 
 */

class Stellar_Nav_Menu_Item_Custom_Fields {

	/**
	 * Default Setup for Menus
	 *
	 * Adds our walker filter and hooks 'save_post'
	 *
	 */
	static function setup() {
		
		// If we're not in admin, don't bother doing setup
		if ( !is_admin() ) {
			return;
		}

		// Add our walker Filter
		// We'll use this to inject our fields
		add_filter( 'wp_edit_nav_menu_walker', function () {
			return 'Stellar_Walker_Nav_Menu_Edit';
		});

		// Hook our save_post to save our custom fields
		add_action( 'save_post', array( __CLASS__, '_save_post' ), 10, 2 );
	}

	/**
	 * Inject the
	 * @hook {action} for our fields
	 */
	static function get_menu_item_fields( $item, $depth, $args ) {

		$instance = get_post_meta( $item->ID );

		foreach( $instance as $key => $value ) {
			$instance[$key] = $value[0];
		}

		$args = array(
			'fields' => \BrightFire\Theme\Stellar\stellar_define_custom_fields( $item->ID ),
			'instance' => $instance,
			'display' => 'basic',
			'echo' => false,
		);
		$stellar_fields = BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

		return $stellar_fields;
		
	}

	/**
	 * Save the newly submitted fields
	 * @hook {action} save_post
	 */
	static function _save_post( $post_id, $post ) {

		// Don't update if this save was triggered by the customizer since our stellar menu meta fields are missing
		// from the customize form and make sure we're only hooking nav_menu_item saves
		if ( array_key_exists( 'customized', $_POST ) || $post->post_type !== 'nav_menu_item' ) {
			return $post_id; // prevent weird things from happening
		}

		// Our Fields
		$fields = \BrightFire\Theme\Stellar\stellar_define_custom_fields( $post_id );

		// Update our post meta with $_POST values or default_value
		foreach ( $fields as $key => $field_schema ) {

			if ( isset( $_POST[ $key ][ $post_id ] ) ) {
				$value = stripslashes( $_POST[ $key ][ $post_id ] );
				update_post_meta( $post_id, $key, $value );
			} else {
				$value = $field_schema[ 'default_value' ];
				update_post_meta( $post_id, $key, $value );
			}
		}
	}

}

/**
 * Walker For Nav Menu Edit screen in Admin
 *
 * injects our custom field markup into each menu-item edit form
 * just before the options to move fields
 *
 * Extends the parent WP Walker and calls it's parent function to generate markup as normal.
 * Uses preg_replace on class = "field-move" to decide where to inject ( see line: 29 )
 *
 */

// We Need This
require_once ABSPATH . 'wp-admin/includes/nav-menu.php';


class Stellar_Walker_Nav_Menu_Edit extends Walker_Nav_Menu_Edit {
	function start_el( &$output, $item, $depth = 0, $args = Array(), $id = 0 ) {
		$item_output = '';

		// Get markup from parent walker
		parent::start_el($item_output, $item, $depth, $args);

		// Menu Item Options
		if ( $menu_item_fields = Stellar_Nav_Menu_Item_Custom_Fields::get_menu_item_fields( $item, $depth, $args ) ) {
			$uniqid = uniqid();

			$open = '<div class="clear"></div><div class="stellar-custom-options" id="stellar-options-' . $uniqid . '">' . '<h4>Advanced Options</h4>';
			$close = '<div class="clear"></div></div>';
			$js = '<script type="text/javascript">
				if( typeof init_child_selectize == "function" ) {
					init_child_selectize( $(\'#stellar-options-' . $uniqid . '\') );
				}</script>';

			$item_output = preg_replace('/(?=<p[^>]+class="[^"]*field-move)/', $open . $menu_item_fields . $close . $js, $item_output);
		}

		$output .= $item_output;
	}
}