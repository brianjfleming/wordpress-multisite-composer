<?php

/**
 * BrightFire Menu Bar widget class
 */
class BrightFire_Menu_Bar extends WP_Widget {

    private $defaults;

	public function __construct() {

		$this->defaults = array(
			'title' => '',
			'menu_id' => '',
			'mobile_menu_text' => 'MENU',
			'menu_style' => 'justify',
            'text_menu'         => 'dark',
            'text_hover'        => 'light',
            'background_hover'  => 'accent',
			'drop_text_hover'        => 'light',
			'drop_background_hover'  => 'dark',
		);

		$widget_ops = array(
			'classname'   => 'brightfire-menu-bar',
			'description' => __( 'Mega menus for Stellar theme.' )
		);

		parent::__construct( 'brightfire_menu_bar', __( 'BrightFire Menu - Bar & Mega menu' ), $widget_ops );
	}

	// Front end display
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		echo $args['before_widget'];

		if( $instance[ 'title' ] ) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}

		\BrightFire\Theme\Stellar\stellar_custom_menu( $instance );

		echo $args['after_widget'];

	}

	// Admin display
	public function form( $instance ) {

		//Defaults
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		\BF_Admin_API_Fields::bf_admin_api_fields_build(
			array(
				'fields' => array(
					'title' => array(
						'label'       => 'Title',
						'type'        => 'text',
					),
					'mobile_menu_text' => array(
						'label'       => 'Mobile Menu Text',
						'type'        => 'text',
					),
					'menu_id' => array(
						'label'       => 'Select Menu',
						'type'        => 'select',
						'choices'     => $this->get_menus()
					),
					'menu_style' => array(
						'label'       => 'Select Desktop Menu Style',
						'type'        => 'select',
						'choices'     => array(
							'left' => 'Left Align',
							'right'  => 'Right Align',
							'center'  => 'Center Align',
							'justify'  => 'Justify',
							'justify-spaced' =>'Justify Spaced',
						)
					),
					'text_menu' => array(
						'label'       => 'Text Color',
						'type'        => 'select',
						'choices'     => $this->color_choices(),
					),
					'text_hover' => array(
						'label'       => 'Text Hover Color',
						'type'        => 'select',
						'choices'     => $this->color_choices()
					),
					'background_hover' => array(
						'label'       => 'Background Hover Color',
						'type'        => 'select',
						'choices'     => $this->color_choices()
					),
					'drop_text_hover' => array(
						'label'       => 'Dropdown Text Hover Color',
						'type'        => 'select',
						'choices'     => $this->color_choices()
					),
					'drop_background_hover' => array(
						'label'       => 'Dropdown Background Hover Color',
						'type'        => 'select',
						'choices'     => $this->color_choices()
					),
				),
				'instance' => $instance,
				'display' => 'basic',
				'widget_instance' => $this,
				'echo' => true,
			)
		);
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $new_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}

	public function get_menus() {

	    $menus = [];

	    $all_menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );

		foreach ( $all_menus as $menu ) {
		    $menus[ $menu->term_id ] = $menu->name;
        }

		return $menus;

	}

	public function color_choices() {

	    return [
	        'primary'   => 'Primary',
            'secondary' => 'Secondary',
            'tertiary'  => 'Tertiary',
            'accent'    => 'Accent',
            'light'     => 'Light',
            'gray'      => 'Gray',
            'dark'      => 'Dark',
		    'custom1'   => 'Custom 1',
		    'custom2'   => 'Custom 2'
        ];
    }
}

// Register widget
add_action( 'widgets_init', function () {
	unregister_widget( 'WP_Nav_Menu_Widget' );
	register_widget( 'BrightFire_Menu_Bar' );
} );