<?php

/**
 * BrightFire Menu Stacked widget class
 */
class BrightFire_Menu_Stacked extends WP_Widget {

	public function __construct() {

		$this->defaults = array(
			'title' => '',
			'menu_id' => '',
			'menu_display' => 'stacked',
			
		);

		$widget_ops = array(
			'classname'   => 'brightfire-menu-stacked',
			'description' => __( 'Stacked Menu for Stellar theme.' )
		);

		parent::__construct( 'brightfire_menu_stacked', __( 'BrightFire Menu - Stacked' ), $widget_ops );

	}

	// Front end display
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		echo $args['before_widget'];

		if( $instance[ 'title' ] ) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}

		\BrightFire\Theme\Stellar\stellar_custom_menu( $instance );

		echo $args['after_widget'];

	}

	// Admin display
	public function form( $instance ) {

		//Defaults
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'stellar' ); ?></label>
			<input type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" id="<?php echo $this->get_field_id( 'title' ); ?>" value="<?php echo $instance['title']; ?>">

		</p>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'menu_id' ); ?>"><?php _e( 'Select Menu', 'stellar' ); ?></label>

			<select name="<?php echo $this->get_field_name( 'menu_id' ); ?>" id="<?php echo $this->get_field_id( 'menu_id' ); ?>">
				<option value="">-- Select a Menu to Display--</option>
				<?php

				$menus = $this->get_menus();

				foreach ( $menus as $menu ) {
					echo '<option value="' . $menu->term_id . '" ' . selected( $instance['menu_id'], $menu->term_id ) . '>' . $menu->name . '</option>';
				}

				?>
			</select>
		</p>

		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $new_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );
		return $instance;
	}

	public function get_menus() {

		$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );

		return $menus;

	}
}

// Register widget
add_action( 'widgets_init', function () {
	unregister_widget( 'WP_Nav_Menu_Widget' );
	register_widget( 'BrightFire_Menu_Stacked' );
} );