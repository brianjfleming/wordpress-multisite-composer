<?php

namespace BrightFire\Theme\Stellar;

require_once BF_STELLAR_INC . 'menus/_class.stellar-menus-admin.php';
require_once BF_STELLAR_INC . 'menus/_stellar-menus-define-custom-fields.php';
require_once BF_STELLAR_INC . 'menus/_widget.stellar-menu-bar.php';
require_once BF_STELLAR_INC . 'menus/_widget.stellar-menu-button-bar.php';
require_once BF_STELLAR_INC . 'menus/_widget.stellar-menu-inline.php';
require_once BF_STELLAR_INC . 'menus/_widget.stellar-menu-stacked.php';
require_once BF_STELLAR_INC . 'menus/_shortcode.stellar-menus.php';
require_once BF_STELLAR_INC . 'menus/actions.php';

/** Custom Fields for Menu Items */
add_action( 'init', array( 'Stellar_Nav_Menu_Item_Custom_Fields', 'setup' ) );

/** Add the Stellar Menu Item as a custom Item type
 *
 * TODO: Should we remove this? Is it worth having or does it create confusion?
 *
 **/
add_action( 'admin_init', __NAMESPACE__ . '\stellar_menu_item_add' );
