<?php

namespace BrightFire\Theme\Stellar;


if ( ! class_exists( 'WP_Widget' ) ) {
	return;
}


/**
 * Class BrightFire_Form_Widget
 */
class BrightFire_Form_Widget extends \WP_Widget {

	public $defaults = array(
		'title'             => '',
		'show_form_title'   => 'no',
		'form_id'           => '0',
		'labels'       => 'top',
		'placeholders' => 'hide',
		'form_layout'       => 'default',
		'form_footer_inline' => 'no'
	);

	function __construct() {

		// Setup Parent Globals, etc
		$widget_ops = array(
			'classname'                   => 'brightfire-form-widget',
			'description'                 => __( 'Displays GravityForm with additional display and layout options' ),
		);

		parent::__construct( 'brightfire_form_widget', __( 'BrightFire Form Widget' ), $widget_ops );
	}

	public function widget( $args, $instance ) {

		// Parse our instance into our defaults
		$instance  = wp_parse_args( $instance, $this->defaults );
		$class_arr = array();

		// FORM LOGIC
		$form_markup = '';
		$ajax = is_customize_preview() ? 'false' : 'true';
		if( 0 == $instance['form_id'] ) {
			$object_id = get_queried_object_id();

			$dynamic_form = get_post_meta( $object_id, 'post_form_id' );

			if( ! empty($dynamic_form) && 0 != $dynamic_form[0] ) {
				$form_id = $dynamic_form[0]; // Set for later use
				$form_markup .= do_shortcode( '[gravityform id="' . $dynamic_form[0] . '" title="false" description="false" ajax="' . $ajax . '"]' );
			} else {
				// Dynamic form requested - none set. Ignore and return
				return false;
			}
		}
		elseif ( 0 != $instance['form_id'] && false !== \GFAPI::get_form( $instance[ 'form_id' ] ) ) {
			$form_id = $instance['form_id'];
			$form_markup .= do_shortcode( '[gravityform id="' . $instance['form_id'] . '" title="false" description="false" ajax="' . $ajax . '"]' );
		} else {
			$form_id = 0;
			$form_markup .= \BrightFire\Library\BFCore\format_bf_error( 'Invalid Form ID' );
		}

		// Hide Placeholders
		if ( 'hide' == $instance['placeholders'] ) {
			$class_arr[] = 'hide_placeholders';
		}

		// Hide Labels
		if ( 'hide' == $instance['labels'] ) {
			$class_arr[] = 'hide_labels';
		} else if ( 'left' == $instance[ 'labels' ] ) {
			$class_arr[] = 'left_labels';
		} else if ( 'top' == $instance[ 'labels' ] ) {
			$class_arr[] = 'top_labels'; // Due Dillegence - this is default styling
		}

		// Layout
		if ( 'default' != $instance['form_layout'] ) {
			// Count GForm fields
			$layout_cols = $instance[ 'form_layout' ];

			if( ( 'yes' == $instance[ 'form_footer_inline' ] ) ) {
				$class_arr[] = 'inline-button';
			}

			$class_arr[] = 'list_col' . $layout_cols;
		}

		// Setup our classes properly
		$class_str             = implode( ' ', $class_arr );
		$args['before_widget'] = preg_replace( '/class="/', "class=\"$class_str ", $args['before_widget'], 1 );


		// BEFORE WIDGET
		echo $args['before_widget'];

		// TITLE
		$title = '';
		if ( ! empty( $instance['title'] ) ) {
			$title = $instance['title'];
		} elseif ( 'yes' == $instance['show_form_title'] ) {
			$form = \GFAPI::get_form( $form_id );
			$title = $form['title'];
		}

		// Output Title
		if( '' != $title ) {
			echo $args['before_title'] . do_shortcode( $title ) . $args['after_title'];
		}

		// DISPLAY FORM
		echo $form_markup;

		// AFTER WIDGET
		echo $args['after_widget'];

	}

	public function form( $instance ) {

		// Get our Fields
		$fields = widget_define_fields();
		$instance  = wp_parse_args( $instance, $this->defaults );

		// Build our widget form
		$args = array(
			'fields'          => $fields,
			'display'         => 'basic',
			'echo'            => true,
			'widget_instance' => $this,
			'instance'        => $instance
		);
		\BF_Admin_API_Fields::bf_admin_api_fields_build( $args );

	}

	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']             = $new_instance['title'];      // Widget Title
		$instance['show_form_title']             = $new_instance['show_form_title'];      // Widget Title
		$instance['form_id']           = $new_instance['form_id'];      // Form ID
		$instance['labels']           = $new_instance['labels'];      // Form ID
		$instance['placeholders']           = $new_instance['placeholders'];      // Form ID
		$instance['form_layout']       = $new_instance['form_layout'];      // Form layout
		$instance['form_footer_inline']       = $new_instance['form_footer_inline'];      // Form layout

		return $instance;
	}


}

/**
 * Widget Form Fields Define
 * @return array
 */
function widget_define_fields() {
	return array(
		'title'             => array(
			'type'   => 'text',
			'label'  => 'Title',
			'atts'   => array(
				'placeholder' => 'No Title',
			),
			'permit' => 1
		),
		'form_id'           => array(
			'type'    => 'selectize',
			'label'   => 'Select Form',
			'choices' => widget_get_forms(),
			'permit'  => 1
		),
		'show_form_title' => array(
			'type' => 'selectize',
			'label' => 'Show Form Title',
			'choices' => array(
				'yes' => 'Yes',
				'no' => 'No',
			),
			'permit' => 1
		),
		'labels'       => array(
			'type'    => 'selectize',
			'label'   => 'Labels ',
			'choices' => array(
				'hide'   => 'Hide Labels',
				'left' => 'Left Labels',
				'top'  => 'Top Labels'
			),
			'permit'  => 1
		),
		'placeholders'       => array(
			'type'    => 'selectize',
			'label'   => 'Placeholders',
			'choices' => array(
				'hide' => 'Hide Placeholders',
				'show'  => 'Show Placeholders'
			),
			'permit'  => 1
		),
		'form_layout'       => array(
			'type'    => 'selectize',
			'label'   => 'Form Layout',
			'choices' => array(
				'default'   => 'Default (Stacked)',
				'2' => '2 Columns',
				'3' => '3 Columns',
				'4' => '4 Columns',
				'5' => '5 Columns',
			),
			'permit'  => 1
		),
		'form_footer_inline' => array(
			'type' => 'selectize',
			'label' => 'Display Submit Inline',
			'choices' => array(
				'yes' => 'Yes',
				'no' => 'No',
			),
			'permit' => 1
		)
	);
}

function widget_get_forms() {
	$forms     = \RGFormsModel::get_forms( null, 'title' );
	$rtn_forms = array();
	foreach ( $forms as $form ) {
		$rtn_forms[ $form->id ] = $form->title;
	}

	$rtn_forms[0] = 'Dynamic';

	asort( $rtn_forms );

	return $rtn_forms;
}

function widget_get_field_count( $id ) {
	$form = \GFAPI::get_form( $id );
	$fields = count($form['fields']);
	return $fields;
}

/**
 * Registers the widget
 * @return bool
 */
function register_brightfire_form_widget() {
	register_widget( 'BrightFire\Theme\Stellar\BrightFire_Form_Widget' );
}

add_action( 'widgets_init', __NAMESPACE__ . '\register_brightfire_form_widget' );